# frozen_string_literal: true

::FactoryBot.define do
  factory :user, class: User do
    password { '123456' }
    password_confirmation { '123456' }
    confirmed_at { ::DateTime.now }
    email { ::Faker::Internet.safe_email }
    admin { false }
  end

  factory :google_user, parent: :user do
    friendly_token = ::Devise.friendly_token[0, 20]
    display_name { ::Faker::Name.name }
    password { friendly_token }
    password_confirmation { friendly_token }
    email { |u| ::Faker::Internet.safe_email(name: u.display_name) }
    provider { 'google_oauth2' }
    uid { ::SecureRandom.hex(8) }
    admin { false }
  end

  factory :facebook_user, parent: :user do
    friendly_token = ::Devise.friendly_token[0, 20]
    display_name { ::Faker::Name.name }
    password { friendly_token }
    password_confirmation { friendly_token }
    email { |u| ::Faker::Internet.safe_email(name: u.display_name) }
    provider { 'facebook' }
    uid { ::SecureRandom.hex(8) }
    admin { false }
  end

  factory :github_user, parent: :user do
    friendly_token = ::Devise.friendly_token[0, 20]
    display_name { ::Faker::Internet.username }
    password { friendly_token }
    password_confirmation { friendly_token }
    email { "#{::Time.current.to_i}_#{::SecureRandom.hex(10)}@dummy.com" }
    provider { 'github' }
    uid { ::SecureRandom.hex(8) }
    admin { false }
  end

  factory :admin, parent: :user do
    admin { true }
  end

  factory :confirmed_user, parent: :user do
    confirmed_at { ::DateTime.now }
  end
end
