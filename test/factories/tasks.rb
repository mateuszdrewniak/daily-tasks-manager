# frozen_string_literal: true

::FactoryBot.define do
  factory :task do
    minutes { 90 }
    priority { 0 }
    name { ::Faker::Verb.ing_form }
    user { |t| t.association(:user) }
  end
end
