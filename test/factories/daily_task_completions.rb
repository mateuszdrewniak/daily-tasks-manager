# frozen_string_literal: true

::FactoryBot.define do
  factory :daily_task_completion do
    user { |d| d.association(:user) }
    spent_seconds { 5395 } # '01:29:55'
    daily_task { |d| d.association(:daily_task, user: d.user) }
    date { ::Support::Date.today }
  end

  factory :completed_daily_task_completion, parent: :daily_task_completion do
    spent_seconds { 5 }
    completed { true }
  end
end
