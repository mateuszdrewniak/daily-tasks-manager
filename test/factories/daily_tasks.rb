# frozen_string_literal: true

::FactoryBot.define do
  factory :daily_task do
    day { ::Support::Date.weekday(::Time.zone.now) }
    user { |d| d.association(:user) }
    task { |d| d.association(:task, user: d.user) }
  end
end
