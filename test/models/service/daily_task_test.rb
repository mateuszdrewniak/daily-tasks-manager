# frozen_string_literal: true

require 'test_helper'

class Service::DailyTaskTest < ActiveSupport::TestCase
  setup do
    @user = FactoryBot.create(:user)
  end

  test 'create a daily_task' do
    task = FactoryBot.create(:task, user: @user)
    builder = Service::DailyTask.new({ task_id: task.id, day: 4 }, @user)

    builder.create!
    daily_task = builder.persisted_daily_task

    assert_equal 4, daily_task.day
    assert_equal task.id, daily_task.task_id
    assert_equal @user.id, daily_task.user_id
  end

  test 'should not create a daily_task when the task belogns to another user' do
    task = FactoryBot.create(:task)
    builder = Service::DailyTask.new({ task_id: task.id, day: 4 }, @user)

    assert_raises ActiveRecord::RecordNotFound do
      builder.create!
    end
  end

  test 'non-existent task' do
    builder = Service::DailyTask.new({ task_id: 12424545, day: 4 }, @user)

    assert_raises ActiveRecord::RecordNotFound do
      builder.create!
    end

  end

  test 'invalid day' do
    task = FactoryBot.create(:task, user: @user)
    builder = Service::DailyTask.new({ task_id: task.id, day: 10 }, @user)

    assert_raises ActiveRecord::RecordInvalid do
      builder.create!
    end
  end
end
