# frozen_string_literal: true

require 'test_helper'

class Service::TaskTest < ActiveSupport::TestCase
  setup do
    @user = FactoryBot.create(:user)
  end

  test 'create a valid task' do
    builder = Service::Task.new(
      {
        time: '00:20',
        name: 'Something',
        priority_id: '0'
      },
      @user
    )

    assert builder.create!
    assert builder.persisted_task
    task = builder.persisted_task

    assert_equal 0, task.hours
    assert_equal 20, task.minutes
    assert_equal 'Something', task.name
    assert_equal 0, task.priority
  end

  test 'should succesfuly update a task' do
    task = FactoryBot.create(:task, user: @user)
    builder = Service::Task.new(
      {
        id: task.id,
        time: '00:20',
        name: 'Something',
        priority_id: '0'
      },
      @user
    )

    assert builder.update!
    assert builder.persisted_task
    task = builder.persisted_task

    assert_equal 0, task.hours
    assert_equal 20, task.minutes
    assert_equal 'Something', task.name
    assert_equal 0, task.priority
  end

  test 'should not update a task when it belongs to another user' do
    task = FactoryBot.create(:task)
    builder = Service::Task.new(
      {
        id: task.id,
        time: '00:20',
        name: 'Something',
        priority_id: '0'
      },
      @user
    )

    assert_raises ActiveRecord::RecordNotFound do
      assert builder.update!
    end
  end

  test 'should not update a task when it does not exist' do
    builder = Service::Task.new(
      {
        id: 1231412,
        time: '00:20',
        name: 'Something',
        priority_id: '0'
      },
      @user
    )

    assert_raises ActiveRecord::RecordNotFound do
      assert builder.update!
    end
  end

  test 'invalid time' do
    builder = Service::Task.new(
      {
        time: '00:20:0235:2356',
        name: 'Something',
        priority_id: '0'
      },
      @user
    )

    assert_raises ActiveRecord::RecordInvalid do
      assert builder.create!
    end

    builder = Service::Task.new(
      {
        time: '00:70',
        name: 'Something',
        priority_id: '0'
      },
      @user
    )

    assert_raises ActiveRecord::RecordInvalid do
      assert builder.create!
    end

    builder = Service::Task.new(
      {
        time: '31:20',
        name: 'Something',
        priority_id: '0'
      },
      @user
    )

    assert_raises ActiveRecord::RecordInvalid do
      assert builder.create!
    end
  end

  test 'invalid priority' do
    builder = Service::Task.new(
      {
        time: '00:20',
        name: 'Something',
        priority_id: '10'
      },
      @user
    )

    assert_raises ActiveRecord::RecordInvalid do
      assert builder.create!
    end
  end
end
