require 'test_helper'

class UserTest < ActiveSupport::TestCase
  should 'correctly handle timezones' do
    assert User.timezones.is_a? Hash
    assert User.timezones_inverted.is_a? Hash

    assert_equal User.timezones['Midway Island'], 'Pacific/Midway'
    assert_equal User.timezones_inverted['Pacific/Midway'], 'Midway Island'
  end

  should 'return correct level' do
    user = FactoryBot.build(:user)
    assert_equal 0, user.level
    assert_nil user.read_attribute(:level)

    user.level = 3
    assert_equal 3, user.read_attribute(:level)
  end

  should 'create a user from facebook omniauth' do
    uid = ::SecureRandom.hex(8)
    auth = OpenStruct.new(
      provider: 'facebook',
      uid: uid,
      info: OpenStruct.new(
        image: 'image_url',
        name: 'Mati Dupa',
        email: 'mati@dupa.com'
      )
    )

    user = assert_difference 'User.count' do
      User.from_omniauth(auth)
    end

    assert_equal 'image_url', user.avatar_url
    assert user.avatar_source.facebook?
    assert user.account_type.facebook?
    assert_equal 'Mati Dupa', user.display_name
    assert_equal 'mati@dupa.com', user.email
    assert_equal uid, user.uid
    assert_equal 'facebook', user.provider
    assert_not_nil user.password
    assert user.persisted?
  end

  should 'create a user from github omniauth' do
    uid = ::SecureRandom.hex(8)
    auth = OpenStruct.new(
      provider: 'github',
      uid: uid,
      info: OpenStruct.new(
        image: 'image_url',
        name: 'versedi'
      )
    )

    user = assert_difference 'User.count' do
      User.from_omniauth(auth)
    end

    assert_equal 'image_url', user.avatar_url
    assert user.avatar_source.github?
    assert user.account_type.github?
    assert_equal 'versedi', user.display_name
    assert_not_nil user.email
    assert_equal 'dummy.com', user.email.split('@').last
    assert_equal uid, user.uid
    assert_equal 'github', user.provider
    assert_not_nil user.password
    assert user.persisted?
  end

  should 'create a user from google omniauth' do
    uid = ::SecureRandom.hex(8)
    auth = OpenStruct.new(
      provider: 'google',
      uid: uid,
      info: OpenStruct.new(
        image: 'image_url',
        name: 'Mati Dupa',
        email: 'mati@dupa.com'
      )
    )

    user = assert_difference 'User.count' do
      User.from_omniauth(auth)
    end

    assert_equal 'image_url', user.avatar_url
    assert user.avatar_source.google?
    assert user.account_type.google?
    assert_equal 'Mati Dupa', user.display_name
    assert_equal 'mati@dupa.com', user.email
    assert_equal uid, user.uid
    assert_equal 'google', user.provider
    assert_not_nil user.password
    assert user.persisted?
  end

  should 'not create a user from omniauth if email already exists' do
    uid = ::SecureRandom.hex(8)
    auth = OpenStruct.new(
      provider: 'facebook',
      uid: uid,
      info: OpenStruct.new(
        image: 'image_url',
        name: 'Mati Dupa',
        email: 'mati@dupa.com'
      )
    )

    user = assert_difference 'User.count' do
      User.from_omniauth(auth)
    end

    assert user.persisted?

    uid = ::SecureRandom.hex(8)
    auth = OpenStruct.new(
      provider: 'google',
      uid: uid,
      info: OpenStruct.new(
        image: 'image_url',
        name: 'Mati Dupa',
        email: 'mati@dupa.com'
      )
    )

    e = assert_raise User::EmailAlreadyExistsError do
      User.from_omniauth(auth)
    end

    assert_equal 'facebook', e.message
  end

  should 'recognise regular user' do
    user = FactoryBot.build(:user)

    assert_equal 'gravatar', user.avatar_source
    assert user.avatar_source.gravatar?

    user.avatar_url = 'something'
    assert_equal 'gravatar', user.avatar_source
    assert user.avatar_source.gravatar?

    assert_equal 'regular', user.account_type
    assert user.account_type.regular?

    assert_nil user.read_attribute(:display_name)
    assert_not_nil user.display_name
    assert_not_equal user.read_attribute(:display_name), user.display_name
    assert_equal user.email_name, user.display_name
    user.display_name = 'something'
    assert_not_equal user.email_name, user.display_name
    assert_not_nil user.guid
    assert_equal 'usr', user.guid[0..2]

    assert user.auto_timezone
    user.blocked_timezone = true
    assert_not user.auto_timezone
  end

  should 'recognise google user' do
    user = FactoryBot.build(:google_user)

    assert_equal 'gravatar', user.avatar_source
    assert user.avatar_source.gravatar?

    user.avatar_url = 'something'
    assert_equal 'google', user.avatar_source
    assert user.avatar_source.google?

    assert_equal 'google', user.account_type
    assert user.account_type.google?

    assert_not_nil user.read_attribute(:display_name)
    assert_not_nil user.display_name
    assert_equal user.read_attribute(:display_name), user.display_name
    assert_not_nil user.guid
    assert_equal 'usr', user.guid[0..2]

    assert user.auto_timezone
    user.blocked_timezone = true
    assert_not user.auto_timezone
  end

  should 'recognise facebook user' do
    user = FactoryBot.build(:facebook_user)

    assert_equal 'gravatar', user.avatar_source
    assert user.avatar_source.gravatar?

    user.avatar_url = 'something'
    assert_equal 'facebook', user.avatar_source
    assert user.avatar_source.facebook?

    assert_equal 'facebook', user.account_type
    assert user.account_type.facebook?

    assert_not_nil user.read_attribute(:display_name)
    assert_not_nil user.display_name
    assert_equal user.read_attribute(:display_name), user.display_name
    assert_not_nil user.guid
    assert_equal 'usr', user.guid[0..2]

    assert user.auto_timezone
    user.blocked_timezone = true
    assert_not user.auto_timezone
  end

  should 'recognise github user' do
    user = FactoryBot.build(:github_user)

    assert_equal 'gravatar', user.avatar_source
    assert user.avatar_source.gravatar?

    user.avatar_url = 'something'
    assert_equal 'github', user.avatar_source
    assert user.avatar_source.github?

    assert_equal 'github', user.account_type
    assert user.account_type.github?

    assert_not_nil user.read_attribute(:display_name)
    assert_not_nil user.display_name
    assert_equal user.read_attribute(:display_name), user.display_name
    assert_not_nil user.guid
    assert_equal 'usr', user.guid[0..2]

    assert user.auto_timezone
    user.blocked_timezone = true
    assert_not user.auto_timezone
  end
end
