# frozen_string_literal: true

require 'test_helper'

class Support::Simulation::ScheduleTest < ActiveSupport::TestCase
  test 'generate unique task indices' do
    simulator = Support::Simulation::Schedule.new
    simulator.generate_tasks
    @used_indices = []

    simulator.tasks.size.times do |x|
      index = simulator.unique_task_index
      assert_not_nil index, "Run #{x}"
      @used_indices.each do |ind|
        assert_not_equal index, ind, @used_indices
      end
      @used_indices << index
    end

    assert_nil simulator.unique_task_index
  end

  test 'generate daily tasks' do
    simulator = Support::Simulation::Schedule.new
    simulator.generate_tasks
    daily_tasks = simulator.generate_daily_tasks
    @used_ids = []

    daily_tasks.each do |dt|
      assert dt.name.is_a? String
      assert dt.priority.is_a? Integer
      assert dt.time =~ /^\d\d:\d\d$/
      @used_ids.each do |ind|
        assert_not_equal dt.id, ind, @used_inds
      end
      @used_ids << dt.id
    end
  end
end
