require 'test_helper'

class TaskTest < ActiveSupport::TestCase
  def setup
    @user = FactoryBot.create(:user)
  end

  test 'valid object' do
    t = Task.new(minutes: get_minutes(12, 5), name: 'Games', priority: 0, user: @user)
    assert t.valid?
    assert t.save

    assert_equal '12:05', t.time
    t.minutes = get_minutes(8, 25)
    assert_equal '08:25', t.time
  end

  test 'invalid priority' do
    t = Task.new(minutes: get_minutes(20, 5), name: 'Games', priority: -1, user: @user)
    assert_not t.valid?
    assert_not t.save
    assert_equal 'Priority must be greater than or equal to 0', t.errors.full_messages.first

    t = Task.new(minutes: get_minutes(20, 5), name: 'Games', priority: 3, user: @user)
    assert_not t.valid?
    assert_not t.save
    assert_equal 'Priority must be less than 3', t.errors.full_messages.first
  end

  test 'invalid time' do
    t = Task.new(minutes: get_minutes(25, 5), name: 'Games', priority: 0, user: @user)
    assert_not t.valid?
    assert_not t.save
    assert_equal 'Db minutes must be less than 1440', t.errors.full_messages.first

    t = Task.new(minutes: 0, name: 'Games', priority: 0, user: @user)
    assert_not t.valid?
    assert_not t.save
    assert_equal 'Time must be greater than 0', t.errors.full_messages.first
  end

  test 'invalid name' do
    t = Task.new(minutes: get_minutes(22, 5), priority: 0, user: @user)
    assert_not t.valid?
    assert_not t.save
    assert_equal "Name can't be blank", t.errors.full_messages.first
  end

  private

  def get_minutes(hours, minutes)
    hours * 60 + minutes
  end
end
