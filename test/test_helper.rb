::ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'
require 'database_cleaner/active_record'
require 'devise/jwt/test_helpers'

::DatabaseCleaner.strategy = :transaction

class ::ActiveSupport::TestCase
  # Run tests in parallel with specified workers
  # parallelize(workers: :number_of_processors)

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  # fixtures :all

  setup do
    ::DatabaseCleaner.start
  end

  teardown do
    ::DatabaseCleaner.clean
  end

  def self.should(*params, &block)
    self.test(*params, &block)
  end
end
