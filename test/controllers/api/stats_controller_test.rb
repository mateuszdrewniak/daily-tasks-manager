require 'test_helper'

class Api::StatsControllerTest < ::ActionDispatch::IntegrationTest
  include ::Devise::Test::IntegrationHelpers
  include ::DateHelper

  setup do
    @user = ::FactoryBot.create(:user)
    headers = { 'Accept' => 'application/json', 'Content-Type' => 'application/json' }
    @auth_headers = ::Devise::JWT::TestHelpers.auth_headers(headers, @user)
  end

  test "render stats with the chart-kit format" do
    travel_to(::Time.zone.local(2021, 5, 30, 21, 50, 55)) # 30.05.2021 21:50:55 Sunday

    gaming_task = create_task(name: 'Gaming', minutes: 120)
    swedish_task = create_task(name: 'Swedish', minutes: 50, priority: 1)
    irish_flute_task = create_task(name: 'Irish Flute', minutes: 75, priority: 1)
    programming_task = create_task(name: 'Programming', minutes: 240, priority: 2)

    daily_tasks = {
      programming: [],
      swedish: [],
      irish_flute: [],
      gaming: []
    }

    travel_to(::Time.zone.local(2021, 5, 23, 21, 50, 55)) # 23.05.2021 21:50:55 Sunday
    # Programming 4h a day - Monday to Friday
    5.times do |i|
      daily_tasks[:programming] << create_daily_task(task: programming_task, day: i)
    end

    # Swedish 50m a day - Monday, Wednesday, Friday
    3.times do |i|
      daily_tasks[:swedish] << create_daily_task(task: swedish_task, day: i * 2)
    end

    # Irish Flute 1h 15m a day - Tuesday, Thursday
    2.times do |i|
      daily_tasks[:irish_flute] << create_daily_task(task: irish_flute_task, day: i * 2 + 1)
    end

    # Gaming 2h a day - Saturday, Sunday
    2.times do |i|
      daily_tasks[:gaming] << create_daily_task(task: gaming_task, day: i + 5)
    end

    # Programming - 1,5h on Monday
    travel_to(::Time.zone.local(2021, 5, 24, 21, 50, 55)) # 24.05.2021 21:50:55 Monday
    create_daily_task_completion(daily_task: daily_tasks[:programming][0], spent_seconds: 5_400)
    # Programming - 5h on Tuesday
    travel_to(::Time.zone.local(2021, 5, 25, 21, 50, 55)) # 25.05.2021 21:50:55 Tuesday
    create_daily_task_completion(daily_task: daily_tasks[:programming][1], spent_seconds: 18_000, completed: true)
    # Programming - 4h on Wednesday
    travel_to(::Time.zone.local(2021, 5, 26, 21, 50, 55)) # 26.05.2021 21:50:55 Wednesday
    create_daily_task_completion(daily_task: daily_tasks[:programming][2], spent_seconds: 14_400, completed: true)
    # Programming - 4h 10m on Thursday
    travel_to(::Time.zone.local(2021, 5, 27, 21, 50, 55)) # 27.05.2021 21:50:55 Thursday
    create_daily_task_completion(daily_task: daily_tasks[:programming][3], spent_seconds: 15_000, completed: true)
    # Programming - 8h on Friday
    travel_to(::Time.zone.local(2021, 5, 28, 21, 50, 55)) # 28.05.2021 21:50:55 Friday
    create_daily_task_completion(daily_task: daily_tasks[:programming][4], spent_seconds: 28_800, completed: true)

    # Swedish - 1h on Monday
    travel_to(::Time.zone.local(2021, 5, 24, 21, 50, 55)) # 24.05.2021 21:50:55 Monday
    create_daily_task_completion(daily_task: daily_tasks[:swedish][0], spent_seconds: 3_600, completed: true)

    # Irish Flute - 2h on Thursday
    travel_to(::Time.zone.local(2021, 5, 27, 21, 50, 55)) # 27.05.2021 21:50:55 Thursday
    create_daily_task_completion(daily_task: daily_tasks[:irish_flute][1], spent_seconds: 7_200, completed: true)

    # Gaming - 6h on Saturday
    travel_to(::Time.zone.local(2021, 5, 29, 21, 50, 55)) # 29.05.2021 21:50:55 Saturday
    create_daily_task_completion(daily_task: daily_tasks[:gaming][0], spent_seconds: 21_600, completed: true)

    travel_to(::Time.zone.local(2021, 5, 30, 21, 50, 55)) # 30.05.2021 21:50:55 Sunday
    assert_no_difference 'DailyTaskCompletion.count + DailyTask.count + Task.count' do
      get show_stats_url, headers: @auth_headers, as: :json
    end

    assert_equal 200, @response.code.to_i
    response = ::JSON.parse(@response.body)

    # Tasks Count per Priority
    assert response['tasks_count_per_priority'].is_a? ::Array
    assert_equal 3, response['tasks_count_per_priority'].size
    model_response = [{"name"=>"Low", "count"=>1, "color"=>"#21ab65"}, {"name"=>"Medium", "count"=>2, "color"=>"#4299e1"}, {"name"=>"High", "count"=>1, "color"=>"#f16135"}]
    assert_equal model_response, response['tasks_count_per_priority']

    # Tasks Per Day

    # Daily Tasks Count
    daily_tasks_count = response['tasks_per_day']['daily_tasks_count']
    model_response = {"labels"=>["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"], "datasets"=>[{"data"=>[2, 2, 2, 2, 2, 1, 1], "legend"=>"Tasks assigned per day"}]}
    assert_equal model_response, daily_tasks_count

    # Assigned Tasks Minutes
    assigned_tasks_minutes = response['tasks_per_day']['assigned_tasks_minutes']
    model_response = {"labels"=>["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"], "datasets"=>[{"data"=>[290, 315, 290, 315, 290, 120, 120], "legend"=>"Minutes of assigned Tasks per day"}]}
    assert_equal model_response, assigned_tasks_minutes

    # Assigned Tasks Hours
    assigned_tasks_hours = response['tasks_per_day']['assigned_tasks_hours']
    model_response = {"labels"=>["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"], "datasets"=>[{"data"=>[4.83, 5.25, 4.83, 5.25, 4.83, 2.0, 2.0], "legend"=>"Hours of assigned Tasks per day"}]}
    assert_equal model_response, assigned_tasks_hours

    # This Week

    # Minutes Spent On Tasks
    minutes_spent_on_tasks = response['this_week']['minutes_spent_on_tasks']
    model_response = {"labels"=>["Mon 05-24", "Tue 05-25", "Wed 05-26", "Thu 05-27", "Fri 05-28", "Sat 05-29", "Sun 05-30"], "datasets"=>[{"data"=>[150, 300, 240, 370, 480, 360, 0], "legend"=>"Minutes spent"}, {"data"=>[290, 315, 290, 315, 290, 120, 120], "legend"=>"Minutes assigned"}]}
    assert_equal model_response, minutes_spent_on_tasks

    # Minutes Spent Per Task
    minutes_spent_per_task = response['this_week']['minutes_spent_per_task']
    model_response = {"labels"=>["Gaming", "Irish Flute", "Programming", "Swedish"], "datasets"=>[{"data"=>[360, 120, 1_360, 60], "legend"=>"Minutes spent"}, {"data"=>[240, 150, 1200, 150], "legend"=>"Minutes assigned"}]}
    assert_equal model_response, minutes_spent_per_task

    # Hours Spent On Tasks
    hours_spent_on_tasks = response['this_week']['hours_spent_on_tasks']
    model_response = {"labels"=>["Mon 05-24", "Tue 05-25", "Wed 05-26", "Thu 05-27", "Fri 05-28", "Sat 05-29", "Sun 05-30"], "datasets"=>[{"data"=>[2.5, 5.0, 4.0, 6.17, 8.0, 6.0, 0.0], "legend"=>"Hours spent"}, {"data"=>[4.83, 5.25, 4.83, 5.25, 4.83, 2.0, 2.0], "legend"=>"Hours assigned"}]}
    assert_equal model_response, hours_spent_on_tasks

    # Hours Spent Per Task
    hours_spent_per_task = response['this_week']['hours_spent_per_task']
    model_response = {"labels"=>["Gaming", "Irish Flute", "Programming", "Swedish"], "datasets"=>[{"data"=>[6.0, 2.0, 22.67, 1.0], "legend"=>"Hours spent"}, {"data"=>[4.0, 2.5, 20.0, 2.5], "legend"=>"Hours assigned"}]}
    assert_equal model_response, hours_spent_per_task

    # Last Week

    # Minutes Spent On Tasks
    minutes_spent_on_tasks = response['last_week']['minutes_spent_on_tasks']
    model_response = {"labels"=>["Mon 05-17", "Tue 05-18", "Wed 05-19", "Thu 05-20", "Fri 05-21", "Sat 05-22", "Sun 05-23"], "datasets"=>[{"data"=>[0, 0, 0, 0, 0, 0, 0], "legend"=>"Minutes spent"}, {"data"=>[290, 315, 290, 315, 290, 120, 120], "legend"=>"Minutes assigned"}]}
    assert_equal model_response, minutes_spent_on_tasks

    # Minutes Spent Per Task
    minutes_spent_per_task = response['last_week']['minutes_spent_per_task']
    model_response = {"labels"=>["Gaming", "Irish Flute", "Programming", "Swedish"], "datasets"=>[{"data"=>[0, 0, 0, 0], "legend"=>"Minutes spent"}, {"data"=>[240, 150, 1200, 150], "legend"=>"Minutes assigned"}]}
    assert_equal model_response, minutes_spent_per_task

    # Hours Spent On Tasks
    hours_spent_on_tasks = response['last_week']['hours_spent_on_tasks']
    model_response = {"labels"=>["Mon 05-17", "Tue 05-18", "Wed 05-19", "Thu 05-20", "Fri 05-21", "Sat 05-22", "Sun 05-23"], "datasets"=>[{"data"=>[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], "legend"=>"Hours spent"}, {"data"=>[4.83, 5.25, 4.83, 5.25, 4.83, 2.0, 2.0], "legend"=>"Hours assigned"}]}
    assert_equal model_response, hours_spent_on_tasks

    # Hours Spent Per Task
    hours_spent_per_task = response['last_week']['hours_spent_per_task']
    model_response = {"labels"=>["Gaming", "Irish Flute", "Programming", "Swedish"], "datasets"=>[{"data"=>[0.0, 0.0, 0.0, 0.0], "legend"=>"Hours spent"}, {"data"=>[4.0, 2.5, 20.0, 2.5], "legend"=>"Hours assigned"}]}
    assert_equal model_response, hours_spent_per_task
  end
  
  private

  def days_of_week_array
    ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
  end

  def create_task(name: ::Faker::Verb.ing_form, minutes: 90, priority: 0, deleted: nil, user: @user)
    @task = ::FactoryBot.create(:task, name: name, minutes: minutes, user: user, priority: priority, deleted: deleted)
  end

  def create_daily_task(day: 0, task: nil, deleted: nil, user: @user)
    @daily_task = task ? ::FactoryBot.create(:daily_task, user: user, task: task, deleted: deleted, day: day) : ::FactoryBot.create(:daily_task, user: user, deleted: deleted, day: day)
  end

  def create_daily_task_completion(completed: false, daily_task: nil, spent_seconds: 5395, user: @user)
    factory = completed ? :completed_daily_task_completion : :daily_task_completion
    @daily_task_completion = ::FactoryBot.create(factory, user: user, daily_task: daily_task, spent_seconds: spent_seconds)
  end

  def show_stats_url
    ::AppRouter.url_json(:api_show_stats)
  end
end
