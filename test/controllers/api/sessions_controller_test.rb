require 'test_helper'

class Api::SessionsControllerTest < ::ActionDispatch::IntegrationTest
  include ::Devise::Test::IntegrationHelpers

  setup do
    @user = ::FactoryBot.create(:user)
  end

  test "not log in when inactive email" do
    @user.confirmed_at = nil
    @user.save!

    params = {
      user: {
        email: @user.email,
        password: '123456'
      }
    }

    post create_session_url, params: params
    assert_equal 401, @response.code.to_i
    response = ::JSON.parse(@response.body)
    assert_equal "You have to confirm your email address before continuing.", response['error']
  end

  test "not log in when incorrect password" do
    params = {
      user: {
        email: @user.email,
        password: '123'
      }
    }

    post create_session_url, params: params
    assert_equal 401, @response.code.to_i
    response = ::JSON.parse(@response.body)
    assert_equal "Invalid Email or password.", response['error']
  end

  test "not log in when incorrect data" do
    params = {
      user: {
        email: 'jkadjflkadjf',
        password: '123'
      }
    }

    post create_session_url, params: params
    assert_equal 401, @response.code.to_i
    response = ::JSON.parse(@response.body)
    assert_equal "Invalid Email or password.", response['error']
  end

  test "log in and obtain JWT" do
    @user.skip_confirmation!
    @user.save!

    params = {
      user: {
        email: @user.email,
        password: '123456'
      }
    }

    post create_session_url, params: params
    assert_equal 201, @response.code.to_i
    response = ::JSON.parse(@response.body)
    assert_not_nil @response.headers['Authorization']
    assert_equal 'Bearer', @response.headers['Authorization'].split.first
    assert_nil response['error']
  end

  test "obtain JWT and destroy it" do
    @user.skip_confirmation!
    @user.save!

    params = {
      user: {
        email: @user.email,
        password: '123456'
      }
    }

    # create JWT token
    post create_session_url, params: params

    assert_equal 201, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert_not_nil @response.headers['Authorization']
    authorization = @response.headers['Authorization'].split
    assert_equal 'Bearer', authorization.first
    assert_nil response['error']

    params = {
      time: '2:30',
      name: 'Dupa',
      priority_id: 0
    }

    # create a task
    post create_task_url, params: params, headers: { "Authorization": "Bearer #{authorization.last}" }
    assert_equal 201, @response.code.to_i
    response = ::JSON.parse(@response.body)
    assert_not_nil response['id']

    # destroy JWT token
    delete destroy_session_url, headers: { "Authorization": "Bearer #{authorization.last}" }
    assert_equal 204, @response.code.to_i

    # attempt to create a task with an invalid token
    post create_task_url, params: params, headers: { "Authorization": "Bearer #{authorization.last}" }

    assert_equal 401, @response.code.to_i
    response = ::JSON.parse(@response.body)
    assert_equal 'revoked token', response['error']
  end

  test "not create a task with a fake JWT token" do
    params = {
      time: '2:30',
      name: 'Dupa',
      priority_id: 0
    }

    post create_task_url, params: params, headers: { "Authorization": "Bearer Dupa123" }
    assert_equal 401, @response.code.to_i
    response = ::JSON.parse(@response.body)
    assert_nil response['id']
    assert_equal 'Not enough or too many segments', response['error']

    post create_task_url, params: params, headers: { "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjYiLCJzY3AiOiJ1c2VyIiwiYXVkIjpudWxsLCJpYXQiOjE2MjE3MDAzMTQsImV4cCI6MTYyMTcwMzkxNCwianRpIjoiMzJjYWMwNGUtYjkwOS00ZGZmLWFhMzItMDZiYjdjOTZkMWEwIn0.-Su7OZvQ0eROGjbungrPieGoIRLcn1ziW2yahdmB-nw" }
    assert_equal 401, @response.code.to_i
    response = ::JSON.parse(@response.body)
    assert_nil response['id']
    assert_equal 'Signature has expired', response['error']
  end

  private

  def create_session_url
    ::AppRouter.url_json(:api_create_session)
  end

  def destroy_session_url
    ::AppRouter.url_json(:api_destroy_session)
  end

  def create_task_url
    ::AppRouter.url_json(:api_create_task)
  end
end
