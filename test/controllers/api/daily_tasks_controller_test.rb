require 'test_helper'

class Api::DailyTasksControllerTest < ::ActionDispatch::IntegrationTest
  include ::Devise::Test::IntegrationHelpers
  include ::DateHelper

  setup do
    @user = ::FactoryBot.create(:user)
    headers = { 'Accept' => 'application/json', 'Content-Type' => 'application/json' }
    @auth_headers = ::Devise::JWT::TestHelpers.auth_headers(headers, @user)
  end

  test "create a new daily_task" do
    create_daily_task

    params = {
      task_id: @task.id,
      day: 6
    }

    assert_difference 'DailyTask.count' do
      post create_daily_task_url, params: params, headers: @auth_headers, as: :json
    end

    assert_equal 201, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert_not_nil response['id']
    assert_equal @task.id, response['task_id']
    assert_equal 6, response['day']
    assert_nil response['deleted']

    dt = ::DailyTask.last
    assert_equal @user.id, dt.user_id
    assert_equal response['id'], dt.id
    assert_equal @task.id, dt.task_id
    assert_equal 6, dt.day
    assert_nil dt.deleted
  end

  test "not create a new daily_task" do
    create_daily_task

    params = {
      task_id: @task.id,
      day: 20
    }

    assert_no_difference 'DailyTask.count' do
      post create_daily_task_url, params: params, headers: @auth_headers, as: :json
    end

    assert_equal 422, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert_equal 'Validation failed: Day must be less than 7', response['message']

    params = {
      task_id: @task.id,
      day: -1
    }

    assert_no_difference 'DailyTask.count' do
      post create_daily_task_url, params: params, headers: @auth_headers, as: :json
    end

    assert_equal 422, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert_equal 'Validation failed: Day must be greater than or equal to 0', response['message']

    params = {
      task_id: 767823648,
      day: 5
    }

    assert_no_difference 'DailyTask.count' do
      post create_daily_task_url, params: params, headers: @auth_headers, as: :json
    end

    assert_equal 404, @response.code.to_i
  end

  test "destroy a daily_task" do
    create_daily_task

    assert_no_difference 'DailyTask.count' do
      delete destroy_daily_task_url(@daily_task.id), headers: @auth_headers, as: :json
    end

    assert_equal 200, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert_equal @daily_task.id, response['id']
    assert_equal @task.id, response['task_id']
    assert_equal @daily_task.day, response['day']
    assert response['deleted']

    @daily_task.reload
    assert @daily_task.deleted
  end

  test "not destroy a daily_task" do
    assert_no_difference 'DailyTask.count' do
      delete destroy_daily_task_url(123), headers: @auth_headers, as: :json
    end

    assert_equal 404, @response.code.to_i
  end
  
  private

  def create_daily_task(user: @user)
    @daily_task = ::FactoryBot.create(:daily_task, user: user)
    @task = @daily_task.task
  end

  def create_daily_task_completion(completed: false, user: @user)
    factory = completed ? :completed_daily_task_completion : :daily_task_completion
    @daily_task_completion = ::FactoryBot.create(factory, user: user)
  end

  def create_daily_task_url
    ::AppRouter.url_json(:api_create_daily_task)
  end

  def destroy_daily_task_url(id)
    ::AppRouter.url_json(:api_destroy_daily_task, { daily_task_id: id })
  end
end
