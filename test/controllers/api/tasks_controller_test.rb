require 'test_helper'

class Api::TasksControllerTest < ::ActionDispatch::IntegrationTest
  include ::Devise::Test::IntegrationHelpers

  setup do
    @user = ::FactoryBot.create(:user)
    headers = { 'Accept' => 'application/json', 'Content-Type' => 'application/json' }
    @auth_headers = ::Devise::JWT::TestHelpers.auth_headers(headers, @user)
  end

  test "create a new task" do
    params = {
      time: '2:30',
      name: 'Dupa',
      priority_id: 0
    }

    assert_difference 'Task.count' do
      post create_task_url, params: params, headers: @auth_headers, as: :json
    end
    assert_equal 201, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert_not_nil response['id']
    assert_equal 'Dupa', response['name']
    assert_equal '02:30', response['time']
    assert_equal 0, response['priority']
    assert_nil response['deleted']

    params = {
      time: '1:00',
      name: 'Dupa',
      priority_id: 0
    }

    assert_difference 'Task.count' do
      post create_task_url, params: params, headers: @auth_headers, as: :json
    end
    assert_equal 201, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert_not_nil response['id']
    assert_equal 'Dupa', response['name']
    assert_equal '01:00', response['time']
    assert_equal 0, response['priority']
    assert_nil response['deleted']
  end

  test "not create an invalid task" do
    params = {
      time: '2:30:0',
      name: 'Dupa',
      priority_id: 0
    }

    assert_no_difference 'Task.count' do
      post create_task_url, params: params, headers: @auth_headers, as: :json
    end
    assert_equal 422, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert_nil response['id']
    assert_equal 'Validation failed: Time must be greater than 0', response['message']

    params = {
      time: '2:30',
      name: nil,
      priority_id: 0
    }

    assert_no_difference 'Task.count' do
      post create_task_url, params: params, headers: @auth_headers, as: :json
    end
    assert_equal 422, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert_nil response['id']
    assert_equal "Validation failed: Name can't be blank", response['message']

    params = {
      time: '2:30',
      name: 'Dupa',
      priority_id: 5
    }

    assert_no_difference 'Task.count' do
      post create_task_url, params: params, headers: @auth_headers, as: :json
    end
    assert_equal 422, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert_nil response['id']
    assert_equal 'Validation failed: Priority must be less than 3', response['message']
  end

  test "update a task" do
    create_task

    verify_no_change_in_task

    params = {
      time: '2:30',
      name: 'Dupa',
      priority_id: 1
    }

    assert_no_difference 'Task.count' do
      put update_task_url(@task.id), params: params, headers: @auth_headers, as: :json
    end
    assert_equal 200, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert_equal @task.id, response['id']
    assert_equal 'Dupa', response['name']
    assert_equal '02:30', response['time']
    assert_equal 1, response['priority']
    assert_nil response['deleted']
  end

  test "not update a task" do
    create_task

    params = {
      time: '2:30:00',
      name: 'Dupa',
      priority_id: 1
    }

    assert_no_difference 'Task.count' do
      put update_task_url(@task.id), params: params, headers: @auth_headers, as: :json
    end
    assert_equal 422, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert_not_nil response['message']
    assert_equal 'Validation failed: Time must be greater than 0', response['message']

    verify_no_change_in_task

    params = {
      time: '2:30',
      name: nil,
      priority_id: 1
    }

    assert_no_difference 'Task.count' do
      put update_task_url(@task.id), params: params, headers: @auth_headers, as: :json
    end
    assert_equal 422, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert_not_nil response['message']
    assert_equal "Validation failed: Name can't be blank", response['message']

    verify_no_change_in_task

    params = {
      time: '2:30',
      name: 'Dupa',
      priority_id: -1
    }

    put update_task_url(@task.id), params: params, headers: @auth_headers, as: :json
    assert_equal 422, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert_not_nil response['message']
    assert_equal "Validation failed: Priority must be greater than or equal to 0", response['message']

    verify_no_change_in_task
  end

  test "destroy a task" do
    create_task

    assert_no_difference 'Task.count' do
      delete destroy_task_url(@task.id), headers: @auth_headers, as: :json
    end
    assert_equal 200, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert_equal @task.id, response['id']
    assert_equal @task.name, response['name']
    assert_equal @task.time, response['time']
    assert_equal @task.priority, response['priority']
    assert response['deleted']

    @task.reload
    assert @task.deleted
  end

  test "not destroy a task" do
    assert_no_difference 'Task.count' do
      delete destroy_task_url(123562161236), headers: @auth_headers, as: :json
    end
    assert_equal 404, @response.code.to_i
  end
  
  private

  def verify_no_change_in_task(name: 'Dupa', minutes: 90, priority: 0, deleted: nil)
    assert_not_equal name, @task.name
    assert_equal minutes, @task.db_minutes
    assert_equal priority, @task.priority

    if deleted.nil?
      assert_nil deleted
    else
      assert_equal deleted, @task.deleted
    end
  end

  def create_task
    @task = FactoryBot.create(:task, user: @user)
  end

  def create_task_url
    ::AppRouter.url_json(:api_create_task)
  end

  def update_task_url(id)
    ::AppRouter.url_json(:api_update_task, { id: id })
  end

  def destroy_task_url(id)
    ::AppRouter.url_json(:api_destroy_task, { task_id: id })
  end
end
