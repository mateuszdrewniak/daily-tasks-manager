require 'test_helper'

class Api::UsersControllerTest < ::ActionDispatch::IntegrationTest
  include ::Devise::Test::IntegrationHelpers

  setup do
    @user = ::FactoryBot.create(:user)
    headers = { 'Accept' => 'application/json', 'Content-Type' => 'application/json' }
    @auth_headers = ::Devise::JWT::TestHelpers.auth_headers(headers, @user)
  end

  test "show user" do
    get show_user_url, headers: @auth_headers, as: :json
    assert_equal 200, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert response['auto_timezone']
    assert_nil response['timezone']
    assert_nil response['friendly_timezone']
    assert_equal @user.guid, response['guid']
    assert_equal @user.email, response['email']
    assert_equal @user.display_name, response['display_name']
    assert_equal 0, response['level']
    assert_equal @user.avatar_url, response['avatar_url']
    assert_equal 'regular', response['account_type']
    assert_equal 'gravatar', response['avatar_source']
    assert_nil response['email_change']
  end

  test "update user with a new email" do
    params = {
      user: {
        display_name: 'Matik',
        email: 'Dupcia@gmail.com',
        auto_timezone: true,
        timezone: 'Asia/Muscat'
      }
    }

    assert_not_equal 'Matik', @user.display_name
    assert_not_equal 'Dupcia@gmail.com', @user.email
    assert_nil @user.timezone

    put update_user_url, params: params, headers: @auth_headers, as: :json
    assert_equal 200, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert response['auto_timezone']
    assert_nil response['timezone']
    assert_nil response['friendly_timezone']
    assert_equal @user.guid, response['guid']
    assert_not_equal 'Dupcia@gmail.com', response['email']
    assert_equal 'Matik', response['display_name']
    assert_equal 0, response['level']
    assert_not_nil response['avatar_url']
    assert_equal 'regular', response['account_type']
    assert_equal 'gravatar', response['avatar_source']
    assert response['email_change']

    @user.reload
    assert_equal 'Matik', @user.display_name
    assert_not_equal 'Dupcia@gmail.com', @user.email
    assert_nil @user.timezone
  end

  test "update user with a new timezone" do
    params = {
      user: {
        display_name: 'Matik',
        email: @user.email,
        auto_timezone: false,
        timezone: 'Asia/Muscat'
      }
    }

    assert_not_equal 'Matik', @user.display_name
    assert_nil @user.timezone
    assert @user.auto_timezone

    put update_user_url, params: params, headers: @auth_headers, as: :json
    assert_equal 200, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert_not response['auto_timezone']
    assert_equal 'Asia/Muscat', response['timezone']
    assert_equal 'Muscat', response['friendly_timezone']
    assert_equal @user.guid, response['guid']
    assert_equal @user.email, response['email']
    assert_equal 'Matik', response['display_name']
    assert_equal 0, response['level']
    assert_not_nil response['avatar_url']
    assert_equal 'regular', response['account_type']
    assert_equal 'gravatar', response['avatar_source']
    assert_not response['email_change']

    @user.reload
    assert_equal 'Matik', @user.display_name
    assert_not @user.auto_timezone
    assert_equal 'Asia/Muscat', @user.timezone
  end
  
  private

  def show_user_url
    ::AppRouter.url_json(:api_show_user)
  end

  def update_user_url
    ::AppRouter.url_json(:api_update_user)
  end

  def create_task_url
    ::AppRouter.url_json(:api_create_task)
  end
end
