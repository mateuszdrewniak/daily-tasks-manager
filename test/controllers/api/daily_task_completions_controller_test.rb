require 'test_helper'

class Api::DailyTaskCompletionsControllerTest < ::ActionDispatch::IntegrationTest
  include ::Devise::Test::IntegrationHelpers
  include ::DateHelper

  setup do
    @user = ::FactoryBot.create(:user)
    headers = { 'Accept' => 'application/json', 'Content-Type' => 'application/json' }
    @auth_headers = ::Devise::JWT::TestHelpers.auth_headers(headers, @user)
  end

  test "create a new daily_task_completion" do
    create_daily_task

    params = {
      date: today,
      spent_time: '1:23:00',
      completed: false
    }

    assert_difference 'DailyTaskCompletion.count' do
      put update_daily_task_completion_url(@daily_task.id), params: params, headers: @auth_headers, as: :json
    end

    assert_equal 201, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert_not_nil response['id']
    assert_equal ::DailyTaskCompletion.last.id, response['id']
    assert_equal today.split('-').map(&:to_i), response['date'].split('-').map(&:to_i)
    assert_equal '01:23:00', response['spent_time']
    assert_not_nil response['created_at']
    assert_not_nil response['updated_at']
    assert_nil response['user_id']
    assert_not_nil response['daily_task_id']
    assert_not response['completed']
    assert_equal 4980, response['spent_seconds']
  end

  test "update a daily_task_completion" do
    create_daily_task_completion

    params = {
      date: today,
      spent_time: '1:31:11',
      completed: true
    }

    assert_equal @user, @daily_task_completion.user
    assert_equal '01:29:55', @daily_task_completion.spent_time
    assert_not @daily_task_completion.completed

    assert_no_difference 'DailyTaskCompletion.count' do
      put update_daily_task_completion_url(@daily_task.id), params: params, headers: @auth_headers, as: :json
    end

    assert_equal 200, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert_equal @daily_task_completion.id, response['id']
    assert_equal today.split('-').map(&:to_i), response['date'].split('-').map(&:to_i)
    assert_equal '01:31:11', response['spent_time']
    assert_equal @daily_task_completion.created_at.as_json, response['created_at']
    assert_not_equal @daily_task_completion.updated_at.as_json, response['updated_at']
    assert_equal @daily_task_completion.daily_task_id, response['daily_task_id']
    assert response['completed']

    @daily_task_completion.reload

    assert_equal '01:31:11', @daily_task_completion.spent_time
    assert @daily_task_completion.completed
  end

  test "neither update nor create a daily_task" do
    assert_no_difference 'DailyTask.count' do
      put update_daily_task_completion_url(23542523), headers: @auth_headers, as: :json
    end

    assert_equal 404, @response.code.to_i
  end
  
  private

  def create_daily_task
    @daily_task = ::FactoryBot.create(:daily_task, user: @user)
    @task = @daily_task.task
  end

  def create_daily_task_completion(completed: false)
    factory = completed ? :completed_daily_task_completion : :daily_task_completion
    @daily_task_completion = ::FactoryBot.create(factory, user: @user)
    @daily_task = @daily_task_completion.daily_task
  end

  def update_daily_task_completion_url(daily_task_id)
    ::AppRouter.url_json(:api_update_daily_task_completion, { daily_task_id: daily_task_id })
  end
end
