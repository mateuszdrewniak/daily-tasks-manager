require 'test_helper'

class ::Api::TodayControllerTest < ::ActionDispatch::IntegrationTest
  include ::Devise::Test::IntegrationHelpers
  include ::DateHelper

  setup do
    @user = ::FactoryBot.create(:user)
    headers = { 'Accept' => 'application/json', 'Content-Type' => 'application/json' }
    @auth_headers = ::Devise::JWT::TestHelpers.auth_headers(headers, @user)
  end

  test "show today tasks" do
    [@user.local_time.to_date.to_s, nil].each do |date|
      ::DatabaseCleaner.cleaning do
        4.times do
          create_daily_task
        end

        1.times do
          create_daily_task_completion
        end

        2.times do
          create_daily_task_completion(completed: true)
        end

        assert_no_difference 'DailyTask.count' do
          get show_today_url(date), headers: @auth_headers, as: :json
        end

        assert_equal 200, @response.code.to_i
        response = ::JSON.parse(@response.body)

        assert response['todo_tasks'].is_a? Array
        assert_equal 5, response['todo_tasks'].length

        assert response['done_tasks'].is_a? Array
        assert_equal 2, response['done_tasks'].length

        response['todo_tasks'].sort! { |a, b| a['task_id'] <=> b['task_id'] }
        response['done_tasks'].sort! { |a, b| a['task_id'] <=> b['task_id'] }

        daily_task = response['todo_tasks'].first
        assert_nil daily_task['id']
        assert_equal today.split('-').map(&:to_i), daily_task['date'].split('-').map(&:to_i)
        assert daily_task['name'].strip =~ /.*ing$/
        assert_equal '01:30:00', daily_task['time']
        assert_equal 0, daily_task['priority']
        assert_equal '00:00:00', daily_task['spent_time']
        assert_equal false, daily_task['completed']
        assert_equal ::DailyTask.first.id, daily_task['daily_task_id']
        assert_equal ::Task.first.id, daily_task['task_id']
        assert_nil daily_task['created_at']
        assert_nil daily_task['updated_at']

        daily_task_completion = response['done_tasks'].last
        dtc = ::DailyTaskCompletion.last
        assert_equal dtc.id, daily_task_completion['id']
        assert_equal today.split('-').map(&:to_i), daily_task_completion['date'].split('-').map(&:to_i)
        assert daily_task_completion['name'].strip =~ /.*ing$/
        assert_equal '01:30:00', daily_task_completion['time']
        assert_equal 0, daily_task_completion['priority']
        assert_equal '00:00:05', daily_task_completion['spent_time']
        assert_equal true, daily_task_completion['completed']
        assert_equal ::DailyTask.last.id, daily_task_completion['daily_task_id']
        assert_equal ::Task.last.id, daily_task_completion['task_id']
        assert_equal dtc.created_at.as_json, daily_task_completion['created_at']
        assert_equal dtc.updated_at.as_json, daily_task_completion['updated_at']

      end
    end
  end

  test "show empty today tasks" do
    [@user.local_time.to_date.to_s, nil].each do |date|
      assert_no_difference 'DailyTask.count' do
        get show_today_url(date), headers: @auth_headers, as: :json
      end

      assert_equal 200, @response.code.to_i
      response = ::JSON.parse(@response.body)

      assert response['todo_tasks'].is_a? Array
      assert response['todo_tasks'].empty?

      assert response['done_tasks'].is_a? Array
      assert response['done_tasks'].empty?
    end
  end

  test "show today tasks without other users' data" do
    [@user.local_time.to_date.to_s, nil].each do |date|
      ::DatabaseCleaner.cleaning do
        another_user = ::FactoryBot.create(:user)
        create_daily_task(user: another_user)
        2.times { create_daily_task_completion(user: another_user) }
        assert_equal 3, ::Task.count
        assert_equal 3, ::DailyTask.count
        assert_equal 2, ::DailyTaskCompletion.count

        assert_no_difference 'DailyTask.count' do
          get show_today_url(date), headers: @auth_headers, as: :json
        end

        assert_equal 200, @response.code.to_i
        response = ::JSON.parse(@response.body)

        assert response['todo_tasks'].is_a? Array
        assert response['todo_tasks'].empty?

        assert response['done_tasks'].is_a? Array
        assert response['done_tasks'].empty?
      end
    end
  end
  
  private

  def create_daily_task(user: @user)
    @daily_task = ::FactoryBot.create(:daily_task, user: user)
    @task = @daily_task.task
  end

  def create_daily_task_completion(completed: false, user: @user)
    factory = completed ? :completed_daily_task_completion : :daily_task_completion
    @daily_task_completion = ::FactoryBot.create(factory, user: user)
  end

  def show_today_url(date)
    ::AppRouter.url_json(:api_show_today, { date: date })
  end
end
