require 'test_helper'

class ::Api::ScheduleControllerTest < ::ActionDispatch::IntegrationTest
  include ::Devise::Test::IntegrationHelpers
  include ::DateHelper

  setup do
    @user = ::FactoryBot.create(:user)
    headers = { 'Accept' => 'application/json', 'Content-Type' => 'application/json' }
    @auth_headers = ::Devise::JWT::TestHelpers.auth_headers(headers, @user)
  end

  test "show schedule without other users' data" do
    another_user = ::FactoryBot.create(:user)

    create_task(priority: 0, user: another_user)
    create_daily_task(user: another_user)

    assert_no_difference 'DailyTask.count + Task.count' do
      get show_schedule_url, headers: @auth_headers, as: :json
    end

    assert_equal 200, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert response['priorities'].is_a? ::Array
    assert_equal 3, response['priorities'].length
    response['priorities'].each { |priority| assert priority.empty? }

    assert response['days'].is_a? ::Array
    assert_equal 7, response['days'].length
    response['days'].each { |day| assert day.empty? }
  end

  test "show schedule without deleted entities" do
    create_task(priority: 0, deleted: true)
    create_daily_task(day: 0, task: create_task(priority: 0, deleted: true), deleted: true)
    create_daily_task(day: 0, task: create_task(priority: 0), deleted: true)
    create_daily_task(day: 0, task: create_task(priority: 0))

    5.times do
      create_daily_task(day: 1, task: create_task(priority: 1))
    end

    6.times do
      create_daily_task(day: 2, task: create_task(priority: 2))
    end

    assert_equal 15, ::Task.count
    assert_equal 14, ::DailyTask.count

    assert_no_difference 'DailyTask.count + Task.count' do
      get show_schedule_url, headers: @auth_headers, as: :json
    end

    assert_equal 200, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert response['priorities'].is_a? ::Array
    assert_equal 3, response['priorities'].length

    [[0, 2], [1, 5], [2, 6]].each do |priority, count|
      assert response['priorities'][priority].is_a? ::Array
      assert_equal count, response['priorities'][priority].length

      response['priorities'][priority].each do |task|
        assert_not_nil task['id']
        assert task['name'].strip =~ /.*ing$/
        assert task['time'] =~ /^\d{2}:\d{2}$/
        assert_equal priority, task['priority']
        assert_not task['deleted']
      end
    end

    assert response['days'].is_a? ::Array
    assert_equal 7, response['days'].length

    [[0, 1], [1, 5], [2, 6]].each do |day, count|
      assert response['days'][day].is_a? ::Array
      assert_equal count, response['days'][day].length

      response['days'][day].each do |daily_task|
        assert_not_nil daily_task['id']
        assert_equal day, daily_task['day']
        assert_not_nil daily_task['task_id']
        assert_not_nil daily_task['created_at']
        assert_not_nil daily_task['updated_at'] 
        assert_not daily_task['deleted']
      end
    end
  end

  test "show schedule without daily_tasks" do
    create_task(priority: 0)

    2.times do
      create_task(priority: 1)
    end

    4.times do
      create_task(priority: 2)
    end

    assert_no_difference 'DailyTask.count + Task.count' do
      get show_schedule_url, headers: @auth_headers, as: :json
    end

    assert_equal 200, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert response['priorities'].is_a? ::Array
    assert_equal 3, response['priorities'].length

    [[0, 1], [1, 2], [2, 4]].each do |priority, count|
      assert response['priorities'][priority].is_a? ::Array
      assert_equal count, response['priorities'][priority].length

      response['priorities'][priority].each do |task|
        assert_not_nil task['id']
        assert task['name'].strip =~ /.*ing$/
        assert task['time'] =~ /^\d{2}:\d{2}$/
        assert_equal priority, task['priority']
        assert_not task['deleted']
      end
    end

    assert response['days'].is_a? ::Array
    assert_equal 7, response['days'].length

    response['days'].each { |day| assert day.empty? }
  end

  test "show empty schedule" do
    assert_no_difference 'DailyTask.count + Task.count' do
      get show_schedule_url, headers: @auth_headers, as: :json
    end

    assert_equal 200, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert response['priorities'].is_a? ::Array
    assert_equal 3, response['priorities'].length
    response['priorities'].each { |priority| assert priority.empty? }

    assert response['days'].is_a? ::Array
    assert_equal 7, response['days'].length
    response['days'].each { |day| assert day.empty? }
  end
  
  private

  def create_task(priority: 0, deleted: nil, user: @user)
    @task = ::FactoryBot.create(:task, user: user, priority: priority, deleted: deleted)
  end

  def create_daily_task(day: 0, task: nil, deleted: nil, user: @user)
    @daily_task = task ? ::FactoryBot.create(:daily_task, user: user, task: task, deleted: deleted, day: day) : ::FactoryBot.create(:daily_task, user: user, deleted: deleted, day: day)
    @task = @daily_task.task
  end

  def show_schedule_url
    ::AppRouter.url_json(:api_show_schedule)
  end
end
