require 'test_helper'

class ::Api::HistoryControllerTest < ::ActionDispatch::IntegrationTest
  include ::Devise::Test::IntegrationHelpers

  setup do
    travel_to(::Time.zone.local(2021, 5, 26, 21, 50, 55)) # 26.05.2021 21:50:55

    @user = ::FactoryBot.create(:user)
    headers = { 'Accept' => 'application/json', 'Content-Type' => 'application/json' }
    @auth_headers = ::Devise::JWT::TestHelpers.auth_headers(headers, @user)
  end

  # ┌────────────────────┐
  # │ May ░░░░░░░░░ 2021 │
  # ├──┬──┬──┬──┬──┬──┬──┤
  # │Su│Mo│Tu│We│Th│Fr│Sa│
  # ├──┼──┼──┼──┼──┼──┼──┤
  # │░░│░░│░░│░░│░░│░░│01│
  # ├──┼──┼──┼──┼──┼──┼──┤
  # │02│03│04│05│06│07│08│
  # ├──┼──┼──┼──┼──┼──┼──┤
  # │09│10│11│12│13│14│15│
  # ├──┼──┼──┼──┼──┼──┼──┤
  # │16│17│18│19│20│21│22│
  # ├──┼──┼──╔══╗──┼──┼──┤
  # │23│24│25║26║27│28│29│
  # ├──┼──┼──╚══╝──┼──┼──┤
  # │30│31│░░│░░│░░│░░│░░│
  # └──┴──┴──┴──┴──┴──┴──┘

  test "show a month of history" do
    create_task
    create_daily_task

    another_user = ::FactoryBot.create(:user)
    10.times { create_daily_task_completion(date: ::Support::Date.at_beginning_of_month, user: another_user) }

    create_daily_task_completion(date: ::Support::Date.at_beginning_of_month)
    create_daily_task_completion(date: ::Support::Date.at_beginning_of_month, daily_task: create_daily_task(deleted: true))

    3.times { create_daily_task_completion(date: (::Support::Date.at_beginning_of_month.to_datetime + 1)) }
    6.times { create_daily_task_completion(date: ::Support::Date.at_end_of_month) }

    assert_no_difference 'DailyTaskCompletion.count + DailyTask.count + Task.count' do
      get show_history_url, headers: @auth_headers, as: :json
    end

    assert_equal 200, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert response['month'].is_a? ::Hash
    assert response['month']['weeks'].is_a? ::Array
    assert_equal 6, response['month']['weeks'].length
    assert_equal 5, response['month']['number']
    assert_equal 'V', response['month']['roman_number']
    
    # 1st week of the month

    week = response['month']['weeks'][0]
    assert week.is_a? ::Hash
    assert week['days'].is_a? ::Array
    assert_equal 7, week['days'].length

    week['days'][..4].each { |day| assert day.empty? }

    day_number = 1
    week['days'][5..].each do |day|
      assert_equal day_number, day['day_number']
      assert day['weekend']
      assert_equal 5, day['month']
      assert_equal 'V', day['roman_month']
      assert_equal 2021, day['year']
      assert_not day['current']
      assert day['tasks'].is_a? ::Array
      if day_number == 1
        assert_equal(2, day['tasks'].length)
      elsif day_number == 2
        assert_equal(3, day['tasks'].length)
      end

      day_number += 1
    end

    # 2nd, 3rd, 4th, 5th weeks of the month

    response['month']['weeks'][1...-1].each do |week|
      assert week.is_a? ::Hash
      assert week['days'].is_a? ::Array
      assert_equal 7, week['days'].length

      week['days'].each_with_index do |day, day_i|
        assert_equal day_number, day['day_number']
        [5, 6].include?(day_i) ? assert(day['weekend']) : assert_not(day['weekend'])
        assert_equal 5, day['month']
        assert_equal 'V', day['roman_month']
        assert_equal 2021, day['year']
        day['day_number'] == 26 ? assert(day['current']) : assert_not(day['current'])
        assert day['tasks'].is_a? ::Array
        assert day['tasks'].empty?

        day_number += 1
      end
    end

    # last (6th) week of the month

    week = response['month']['weeks'][-1]
    assert week.is_a? ::Hash
    assert week['days'].is_a? ::Array
    assert_equal 7, week['days'].length

    day = week['days'].first

    assert_equal day_number, day['day_number']
    assert_equal 31, day['day_number']
    assert_not day['weekend']
    assert_equal 5, day['month']
    assert_equal 'V', day['roman_month']
    assert_equal 2021, day['year']
    assert_not day['current']
    assert day['tasks'].is_a? ::Array
    assert day['tasks'].empty?

    day_number = 1

    week['days'][1..].each { |day| assert day.empty? }
  end

  test "show three days of history" do
    create_task
    create_daily_task

    another_user = ::FactoryBot.create(:user)
    10.times { create_daily_task_completion(date: ::Support::Date.at_beginning_of_month, user: another_user) }
    10.times { create_daily_task_completion(user: another_user) }

    create_daily_task_completion
    create_daily_task_completion(daily_task: create_daily_task(deleted: true))

    3.times { create_daily_task_completion(date: (::Support::Date.now.to_datetime - 1)) }

    assert_no_difference 'DailyTaskCompletion.count + DailyTask.count + Task.count' do
      get show_history_three_days_url, headers: @auth_headers, as: :json
    end

    assert_equal 200, @response.code.to_i
    response = ::JSON.parse(@response.body)

    assert response['three_days'].is_a? ::Hash
    assert response['three_days']['days'].is_a? ::Array
    assert_equal 3, response['three_days']['days'].length
    
    # 2 days ago

    day = response['three_days']['days'][0]
    assert day.is_a? ::Hash
    assert_equal 24, day['day_number']
    assert_not day['weekend']
    assert_equal 5, day['month']
    assert_equal 'V', day['roman_month']
    assert_equal 2021, day['year']
    assert_not day['current']
    assert day['tasks'].is_a? ::Array
    assert day['tasks'].empty?

    # 1 day ago

    day = response['three_days']['days'][1]
    assert day.is_a? ::Hash
    assert_equal 25, day['day_number']
    assert_not day['weekend']
    assert_equal 5, day['month']
    assert_equal 'V', day['roman_month']
    assert_equal 2021, day['year']
    assert_not day['current']
    assert day['tasks'].is_a? ::Array
    assert_equal 3, day['tasks'].length

    # today

    day = response['three_days']['days'][2]
    assert day.is_a? ::Hash
    assert_equal 26, day['day_number']
    assert_not day['weekend']
    assert_equal 5, day['month']
    assert_equal 'V', day['roman_month']
    assert_equal 2021, day['year']
    assert day['current']
    assert day['tasks'].is_a? ::Array
    assert_equal 2, day['tasks'].length
  end
  
  private

  def now
    ::Time.zone.now
  end

  def month
    now.month
  end

  def roman_month(month_in = month)
    ::Support::Date.roman_month(month_in)
  end

  def weeks_in_the_current_month
    date = now

    (0.5 + (date.at_end_of_month.day + date.at_beginning_of_month.wday).to_f / 7.0).round
  end

  def create_task(priority: 0, deleted: nil, user: @user)
    @task = ::FactoryBot.create(:task, user: user, priority: priority, deleted: deleted)
  end

  def create_daily_task(day: 0, task: nil, deleted: nil, user: @user)
    @daily_task = task ? ::FactoryBot.create(:daily_task, user: user, task: task, deleted: deleted, day: day) : ::FactoryBot.create(:daily_task, user: user, deleted: deleted, day: day)
    @task = @daily_task.task

    @daily_task
  end

  def create_daily_task_completion(completed: false, date: ::Support::Date.today, user: @user, daily_task: ::FactoryBot.create(:daily_task, user: user))
    factory = completed ? :completed_daily_task_completion : :daily_task_completion
    @daily_task_completion = ::FactoryBot.create(factory, user: user, date: date, daily_task: daily_task)
  end

  def show_history_url
    ::AppRouter.url_json(:api_show_history)
  end

  def show_history_three_days_url
    ::AppRouter.url_json(:api_show_history_three_days)
  end
end
