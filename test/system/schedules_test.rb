require 'application_system_test_case'

class SchedulesTest < ApplicationSystemTestCase
  setup do
    @user = FactoryBot.create(:confirmed_user)
  end

  test 'visit schedule, create task and daily_task, delete daily_task and task' do
    login(email: @user.email, password: @user.password)

    click_on 'Schedule'
    assert has_content?('Low')
    assert has_content?('Medium')
    assert has_content?('High')

    find('#add-1').click
    assert has_content?('Create Task')
    fill_in 'name', with: 'Dupa'
    fill_in 'time', with: '5:02'
    click_button 'Create'
    assert has_content?('Dupa')

    task = Task.last
    assert task
    assert_equal 'Dupa', task.name
    assert_equal '05:02', task.time

    task_card = find("#task-#{task.id}")
    tuesday = find("#day-1 .card-group")
    assert_difference 'DailyTask.count' do
      task_card.drag_to(tuesday)
      sleep 1
    end

    task_card = find("#task-#{task.id}")
    wednesday = find("#day-2 .card-group")
    assert_difference 'DailyTask.count' do
      task_card.drag_to(wednesday)
      sleep 1
    end

    assert_no_difference 'DailyTask.count' do
      task_card.drag_to(wednesday)
      sleep 1
    end
    
    daily_task = DailyTask.last
    assert daily_task
    assert_equal task.id, daily_task.task_id
    assert_equal 2, daily_task.day
    assert !daily_task.deleted?

    daily_task_card = find("#task-per-day-#{daily_task.id}")
    monday = find("#day-0 .card-group")
    daily_task_card.drag_to(monday)

    sleep 2
    assert_equal 2, DailyTask.count
    daily_task.reload
    assert daily_task.deleted?

    task_card.click
    assert has_content?('Edit Task')
    find('#edit-task-modal .delete-btn').click

    sleep 2
    daily_task = DailyTask.first
    assert_equal 1, daily_task.day
    assert daily_task.deleted?
  end
end
