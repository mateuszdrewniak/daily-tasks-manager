require 'application_system_test_case'

class TodayTest < ApplicationSystemTestCase
  include DateHelper

  setup do
    @user = FactoryBot.create(:confirmed_user)
    @task_completion = FactoryBot.create(:daily_task_completion, user: @user)
    @task_completed = FactoryBot.create(:completed_daily_task_completion, user: @user)
  end

  test 'visit today and complete started task' do
    login(email: @user.email, password: @user.password)

    assert !@task_completion.completed?

    assert has_content?(today_long)
    assert has_content?('TO DO')
    assert has_content?('DONE')

    assert has_content?(@task_completion.name)
    assert has_content?(@task_completion.spent_time)
    assert has_content?(@task_completed.name)
    assert has_content?(@task_completed.spent_time)

    find("#to-do #task-#{@task_completion.daily_task_id}")
    find("#done #task-#{@task_completed.daily_task_id}")

    find("#start-#{@task_completion.daily_task_id}").click

    sleep 6

    find("#done #task-#{@task_completion.daily_task_id}")
    find("#done #task-#{@task_completed.daily_task_id}")

    @task_completion.reload
    assert @task_completion.completed?
    assert_equal '00:00:00', @task_completion.spent_time

    sleep 5
    find("#stop-#{@task_completion.daily_task_id}").click
    sleep 2
    @task_completion.reload
    assert @task_completion.completed?
    assert_not_equal '00:00:00', @task_completion.spent_time
  end

  test 'visit today and start a new task' do
    @daily_task = FactoryBot.create(:daily_task, user: @user)
    login(email: @user.email, password: @user.password)

    assert_not @task_completion.completed?
    assert_nil @daily_task.completion!(date: today)

    assert has_content?(today_long)
    assert has_content?('TO DO')
    assert has_content?('DONE')

    assert has_content?(@task_completion.name)
    assert has_content?(@task_completion.spent_time)
    assert has_content?(@task_completed.name)
    assert has_content?(@task_completed.spent_time)
    assert has_content?(@daily_task.name)
    assert has_content?(@daily_task.spent_time)

    find("#to-do #task-#{@task_completion.daily_task_id}")
    find("#to-do #task-#{@daily_task.daily_task_id}")
    find("#done #task-#{@task_completed.daily_task_id}")

    find("#start-#{@daily_task.daily_task_id}").click

    sleep 6

    find("#to-do #task-#{@task_completion.daily_task_id}")
    find("#to-do #task-#{@daily_task.daily_task_id}")
    find("#done #task-#{@task_completed.daily_task_id}")

    find("#stop-#{@daily_task.daily_task_id}").click

    sleep 2

    @daily_task.reload
    assert_not @daily_task.completed?
    assert_equal '00:00:00', @daily_task.spent_time

    completion = @daily_task.completion!(date: today)
    assert_not_nil completion
  
    assert_not_equal '00:00:00', completion.spent_time
    assert has_content?(completion.spent_time)
  end
end
