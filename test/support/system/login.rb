# frozen_string_literal: true
module Support
  module System
    module User

      class NoUserError < StandardError
        def message
          'No User in DB!'
        end
      end

      def login(email: , password: )
        visit '/sign_in'
        fill_in 'user_email', with: email
        fill_in 'user_password', with: password
        click_button 'Log in'
      end

      def last_user!
        User.last!
      rescue ActiveRecord::RecordNotFound => e
        raise NoUserError
      end

      def last_user
        User.last
      end
    end
  end
end