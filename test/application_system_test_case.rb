require 'test_helper'
require './test/support/system/login'

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  include Support::System::User

  def self.options
    options = {}
    options[:url] = ENV['SELENIUM_REMOTE_URL'] if ENV['SELENIUM_REMOTE_URL']

    options
  end
  
  Capybara.server_host = '0.0.0.0' if ENV['SELENIUM_REMOTE_URL']
  Capybara.server_port = 7654
  
  driven_by :selenium, using: ENV['HEADLESS'] ? :headless_chrome : :chrome, screen_size: [1400, 1400], options: options

  def setup
    if ENV['SELENIUM_REMOTE_URL']
      net = Socket.ip_address_list.detect(&:ipv4_private?)
      ip = net.nil? ? 'localhost' : net.ip_address
      Capybara.app_host = "http://#{ip}"
    end
    super
  end

  def t(param)
    I18n.t(param)
  end
end
