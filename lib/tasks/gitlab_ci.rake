# frozen_string_literal: true

namespace :gitlab_ci do
  desc "Prepare Gitlab CI environment"
  task :prepare_env do
    if Rails.env.test?
      copy_example_files
    else
      puts "Rails environment is incorrect for this task - #{Rails.env}."
    end
  end

  def copy_example_files
    copy_from_example_file 'database'
  end

  def copy_from_example_file(name)
    cp Rails.root.join("config/#{name}.yml.example"), "config/#{name}.yml"
  end
end
