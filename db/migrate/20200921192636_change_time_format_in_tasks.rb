class ChangeTimeFormatInTasks < ActiveRecord::Migration[6.0]
  def change
    change_table :tasks do |t|
      t.remove  :time
      t.integer :hours, limit: 1
      t.integer :minutes, limit: 1
    end
  end
end
