class AddCompletedToDailyTaskCompletions < ActiveRecord::Migration[6.0]
  def change
    add_column :daily_task_completions, :completed, :boolean
  end
end
