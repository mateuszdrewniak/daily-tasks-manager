class AddIndexToTasks < ActiveRecord::Migration[6.0]
  def change
    add_index :tasks, %i[deleted user_id priority]
  end
end
