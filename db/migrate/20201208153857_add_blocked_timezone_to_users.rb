class AddBlockedTimezoneToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :blocked_timezone, :boolean
  end
end
