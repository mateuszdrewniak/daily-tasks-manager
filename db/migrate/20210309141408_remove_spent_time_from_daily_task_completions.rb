class RemoveSpentTimeFromDailyTaskCompletions < ActiveRecord::Migration[6.0]
  def change
    remove_column :daily_task_completions, :spent_time
  end
end
