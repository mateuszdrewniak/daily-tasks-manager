class AddGuidToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :guid, :string
    add_index :users, :guid, unique: true
  end
end
