class CreateDailyTasks < ActiveRecord::Migration[6.0]
  def change
    create_table :daily_tasks do |t|
      t.integer :day, limit: 1
      t.boolean :deleted

      t.timestamps
    end

    add_reference :daily_tasks, :user, foreign_key: true, index: true
    add_reference :daily_tasks, :task, foreign_key: true, index: true
  end
end
