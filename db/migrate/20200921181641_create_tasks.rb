class CreateTasks < ActiveRecord::Migration[6.0]
  def change
    create_table :tasks do |t|
      t.string    :name, limit: 70
      t.string    :time, limit: 5
      t.integer   :priority, limit: 1

      t.timestamps
    end

    add_reference :tasks, :user, foreign_key: true, index: true
  end
end
