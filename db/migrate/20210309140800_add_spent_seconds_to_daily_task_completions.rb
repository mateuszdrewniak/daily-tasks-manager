class AddSpentSecondsToDailyTaskCompletions < ActiveRecord::Migration[6.0]
  def change
    add_column :daily_task_completions, :spent_seconds, :integer, limit: 3
  end
end