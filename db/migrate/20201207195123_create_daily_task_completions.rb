class CreateDailyTaskCompletions < ActiveRecord::Migration[6.0]
  def change
    create_table :daily_task_completions do |t|
      t.string  :spent_time, limit: 8
      t.date    :date     

      t.timestamps
    end

    add_reference :daily_task_completions, :user, foreign_key: true, index: true
    add_reference :daily_task_completions, :daily_task, foreign_key: true, index: true
  end
end
