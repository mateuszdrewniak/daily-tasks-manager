# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.2'

gem 'activeadmin'
gem 'activerecord-session_store'
gem 'autoprefixer-rails'
gem 'bcrypt', '~> 3.1.7'
gem 'bootstrap'
gem 'cancancan'
gem 'chartkick'
gem 'devise', github: 'heartcombo/devise'
gem 'devise-jwt'
gem 'faker'
gem 'font-awesome-sass', '~> 5.12.0'
gem 'groupdate'
gem 'haml'
gem 'haml-rails'
gem 'jbuilder', '~> 2.7'
gem 'kaminari'
gem 'mysql2', '>= 0.4.4'
gem 'omniauth', '~> 1.8'
gem 'omniauth-facebook'
gem 'omniauth-github'
gem 'omniauth-google-oauth2'
gem 'puma', '~> 4.3.8'
gem 'rails', '~> 6.0.3'
gem 'sass-rails', '>= 6'
gem 'simple_form', '~> 5.0.3'
gem 'turbolinks', '~> 5'
gem 'webpacker', '~> 4.0'
gem 'whenever', require: false

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

group :development, :test do
  gem 'bundler-audit'
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'factory_bot_rails'
  gem 'pronto-rubocop', require: false
  gem 'rubocop'
  gem 'solargraph'
end

group :development do
  gem 'letter_opener'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'rvm'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'database_cleaner-active_record'
  gem 'selenium-webdriver'
  gem 'simplecov', require: false
  gem 'webdrivers', require: !ENV['SELENIUM_REMOTE_URL']
  gem 'whenever-test'
end
