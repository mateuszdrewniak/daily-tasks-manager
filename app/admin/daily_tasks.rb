# frozen_string_literal: true

::ActiveAdmin.register ::DailyTask do
  permit_params :user_id, :task_id, :day

  index do
    selectable_column
    id_column
    column :day do |d|
      ::Support::Date.human_day(d.day)
    end
    column :user do |d|
      link_to "User ##{d.user_id}", admin_user_path(d.user_id)
    end
    column :task do |d|
      link_to "Task ##{d.task_id}", admin_task_path(d.task_id)
    end
    column :deleted
    column :created_at
    actions
  end

  show title: :name do
    attributes_table do
      row :day do |d|
        ::Support::Date.human_day(d.day)
      end
      row :priority do |d|
        priority d.priority
        ::Support::Priority.to_human(d.priority)
      end
      row :time
      row :user
      row :task
      row :deleted
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  filter :user_id, as: :numeric
  filter :task_id, as: :numeric
  filter :day, as: :select, collection: ::Support::Date.days_collection
  filter :deleted, as: :boolean
  filter :created_at
  filter :updated_at

  form do |f|
    f.inputs do
      f.input :user_id
      f.input :task_id
      f.input :day, as: :select, collection: ::Support::Date.days_collection
    end
    f.actions do
      f.action :submit, as: :button
    end
  end
end
