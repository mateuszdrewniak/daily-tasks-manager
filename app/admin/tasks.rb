# frozen_string_literal: true

::ActiveAdmin.register ::Task do
  permit_params :user_id, :name, :priority, :minutes
  # actions :index, :show, :edit, :update, :destroy

  index do
    selectable_column
    id_column
    column :name
    column :priority do |u|
      priority u.priority
    end
    column :user_id
    column :time
    column :deleted
    column :created_at
    column :updated_at
    actions
  end

  show title: :name do
    attributes_table do
      row :name
      row :user
      row :priority do |u|
        priority u.priority
        ::Support::Priority.to_human(u.priority)
      end
      row :time
      row :minutes do |u|
        u.minutes(db: true)
      end
      row :deleted
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  sidebar 'Actions', only: :show do
    ul do
      li link_to "Change User", edit_admin_task_path(change_user: true)
    end
  end

  filter :user_id, as: :numeric
  filter :name, as: :string
  filter :priority, as: :select, collection: ::Support::Priority.collection
  filter :created_at
  filter :updated_at
  filter :deleted, as: :boolean
  filter :minutes, as: :numeric

  form do |f|
    f.inputs do
      if params['change_user'].is_a?(::String) && params['change_user'].inquiry.true?
        f.input :user_id
      end
      f.input :name
      f.input :priority, as: :select, collection: ::Support::Priority.collection
      f.input :minutes
    end
    f.actions do
      f.action :submit, as: :button
    end
  end
end
