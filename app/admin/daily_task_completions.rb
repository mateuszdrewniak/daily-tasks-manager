# frozen_string_literal: true

::ActiveAdmin.register ::DailyTaskCompletion do
  actions :index, :show, :destroy

  index do
    selectable_column
    id_column
    column :spent_time do |d|
      d.human_spent_time
    end
    column :completed
    column :date
    column :user do |d|
      link_to "User ##{d.user_id}", admin_user_path(d.user_id)
    end
    column :daily_task do |d|
      link_to "Daily Task ##{d.daily_task_id}", admin_daily_task_path(d.daily_task_id)
    end
    actions
  end

  show title: :name do
    attributes_table do
      row :full_time
      row :spent_time do |d|
        d.human_spent_time
      end
      row :spent_seconds
      row :completed
      row :date
      row :user
      row :daily_task
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  filter :spent_seconds, as: :numeric
  filter :completed
  filter :date
  filter :created_at
  filter :updated_at
end
