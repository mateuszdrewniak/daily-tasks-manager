# frozen_string_literal: true

module Admin
  module Components
    class Priority < ::Arbre::Component
      builder_method :priority

      def build(priority, attributes = {})
        super(attributes)

        color, symbol = ::SymbolHelper.priority_classes(priority, attributes[:centered])

        span class: "priority #{color}" do
          i class: symbol
        end
      end
    end
  end
end
