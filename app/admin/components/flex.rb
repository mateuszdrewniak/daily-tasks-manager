# frozen_string_literal: true

module Admin
  module Components
    class Flex < ::Arbre::Component
      builder_method :flex

      def build(attributes = {})
        attributes[:class] = "flex #{attributes[:class]}"
        super(attributes)
      end
    end
  end
end
