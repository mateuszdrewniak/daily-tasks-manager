# frozen_string_literal: true

module Admin
  module Components
    class Level < ::Arbre::Component
      builder_method :level

      def build(level, attributes = {})
        class_h = ::SymbolHelper.level_class(level)

        attributes[:class] = attributes[:class].present? ? "level-block #{class_h[:background_color][:class]} #{attributes[:class]}" : "level-block #{class_h[:background_color][:class]}"
        attributes[:title] = 'Level | Streak' unless attributes[:title]
        super(attributes)

        i class: "fas fa-#{class_h[:icon]} #{class_h[:color][:class]}"
        span class: class_h[:color][:class] do
          text_node level
        end
      end
    end
  end
end
