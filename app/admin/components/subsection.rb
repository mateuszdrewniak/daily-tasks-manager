# frozen_string_literal: true

module Admin
  module Components
    class Subsection < ::Arbre::Component
      builder_method :subsection

      def build(attributes = {})
        attributes[:class] = "subsection #{attributes[:class]}"
        super(attributes)
      end
    end
  end
end
