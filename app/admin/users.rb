# frozen_string_literal: true

::ActiveAdmin.register ::User do
  permit_params :email, :password, :password_confirmation, :timezone, :level, :admin
  # actions :index, :show, :edit, :update, :destroy

  index do
    selectable_column
    id_column
    column :display_name
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  show title: :display_name do
    attributes_table do
      row 'Avatar' do |u|
        image_tag(u.avatar_url, class: 'avatar') if u.avatar_url.present?
      end
      row :display_name
      row :email
      row :level do |u|
        level u.level
      end
      row :admin
      row :timezone do |u|
        "[#{u.timezone}] #{u.friendly_timezone}"
      end
      row :blocked_timezone
      row :guid
      row :avatar_url
      row :avatar_source
      row :account_type
      row :reset_password_token
      row :reset_password_sent_at
      row :remember_created_at
      row :confirmation_token
      row :confirmed_at
      row :confirmation_sent_at
      row :unconfirmed_email
      row :provider
      row :uid
      row :created_at
      row :updated_at
    end

    panel 'Stats' do
      stats = ::Service::Stats::User.new(resource)
      green = ::Support::Colors::GREEN
      blue = ::Support::Colors::BLUE
      orange = ::Support::Colors::ORANGE

      subsection do
        h2 'Tasks per priority'
        div do
          pie_chart stats.tasks_count_per_priority, donut: true, colors: [green, blue, orange]
        end
      end

      h2 'General'
      flex do
        subsection class: 'w-6/12' do
          column_chart stats.daily_tasks_count_per_day, ytitle: 'Tasks', colors: [orange]
        end

        subsection id: 'assigned_tasks_minutes_per_day_of_week', class: 'w-6/12 minutes-chart' do
          column_chart stats.assigned_tasks_minutes_per_day_of_week, ytitle: 'Minutes', colors: [blue]
        end
      end

      h2 'Last 7 days'
      flex do
        subsection id: 'minutes_spent_on_tasks_last_seven_days', class: 'w-full minutes-chart' do
          column_chart stats.minutes_spent_on_tasks_last_seven_days, ytitle: 'Minutes', colors: [green, blue]
        end
      end

      h2 'This week'
      flex do
        subsection id: 'minutes_spent_per_task_this_week', class: 'w-full minutes-chart' do
          column_chart stats.minutes_spent_on_tasks_this_week, ytitle: 'Minutes', colors: [green, blue]
        end
      end

      flex do
        subsection id: 'minutes_spent_per_task_this_week', class: 'w-full minutes-chart' do
          column_chart stats.minutes_spent_per_task_this_week, ytitle: 'Minutes', colors: [green, blue]
        end
      end

      h2 'Last week'
      flex do
        subsection id: 'minutes_spent_on_tasks_last_week', class: 'w-full minutes-chart' do
          column_chart stats.minutes_spent_on_tasks_last_week, ytitle: 'Minutes', colors: [green, blue]
        end
      end

      flex do
        subsection id: 'minutes_spent_per_task_last_week', class: 'w-full minutes-chart' do
          column_chart stats.minutes_spent_per_task_last_week, ytitle: 'Minutes', colors: [green, blue]
        end
      end
    end

    active_admin_comments
  end

  sidebar 'Actions', only: :show do
    ul do
      li link_to "Reset Password", edit_admin_user_path(reset_password: true)
    end
  end

  filter :id, as: :numeric
  filter :guid, as: :string
  filter :email, as: :string
  filter :timezone, as: :select, collection: ::User.timezones.to_a.sort
  filter :admin, as: :boolean
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs do
      f.input :email
      if params['reset_password'].is_a?(::String) && params['reset_password'].inquiry.true?
        f.input :password
        f.input :password_confirmation
      end
      f.input :timezone, as: :select, collection: ::User.timezones.to_a.sort
      f.input :level
      f.input :admin
    end
    f.actions do
      f.action :submit, as: :button
    end
  end
end
