# frozen_string_literal: true

class ::Users::OmniauthCallbacksController < ::Devise::OmniauthCallbacksController
  # See https://github.com/omniauth/omniauth/wiki/FAQ#rails-session-is-clobbered-after-callback-on-developer-strategy
  skip_before_action :verify_authenticity_token, only: %i[google_oauth2 facebook github]

  def github
    handle_oauth(:github)
  end

  def facebook
    handle_oauth(:facebook)
  end

  def google_oauth2
    handle_oauth(:google)
  end

  def failure
    redirect_to root_path
  end

  private

  def handle_oauth(provider)
    # You need to implement the method below in your model (e.g. app/models/user.rb)
    @user = ::User.from_omniauth(request.env["omniauth.auth"])

    if @user.persisted?
      sign_in_and_redirect @user, event: :authentication # this will throw if @user is not activated
      set_flash_message(:notice, :success, kind: provider.to_s.capitalize) if is_navigational_format?
      cookies.permanent[:last_oauth] = provider
    else
      session["devise.#{provider}_data"] = request.env["omniauth.auth"].except(:extra) # Removing extra as it can overflow some session stores
      flash[:alert] = "There was a problem signing you in through #{provider.to_s.capitalize}. Please register or try signing in later."
      redirect_to sign_in_url
    end
  rescue ::User::EmailAlreadyExistsError => e
    session["devise.#{provider}_data"] = request.env["omniauth.auth"].except(:extra) # Removing extra as it can overflow some session stores
    flash[:alert] = "A #{e.message.capitalize} account with this email already exists!"
    redirect_to sign_in_url
  end
end
