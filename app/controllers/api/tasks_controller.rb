# frozen_string_literal: true

class ::Api::TasksController < ::ApiController
  def create
    builder = ::Service::Task.new(params, current_user)
    task = builder.create!

    render json: task.as_json, status: :created
  end

  def update
    builder = ::Service::Task.new(params, current_user)
    task = builder.update!

    render json: task.as_json, status: :ok
  end

  def destroy
    task = ::Task.where(user: current_user, id: params[:task_id]).first!
    task.soft_delete

    render json: task.as_json, status: :ok
  end
end
