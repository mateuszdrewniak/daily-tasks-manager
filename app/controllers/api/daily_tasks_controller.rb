# frozen_string_literal: true

class ::Api::DailyTasksController < ::ApiController
  def create
    builder = ::Service::DailyTask.new(params, current_user)
    daily_task = builder.create!

    render json: daily_task.as_json(with_task: true), status: :created
  end

  def destroy
    daily_task = ::DailyTask.where(user: current_user, id: params[:daily_task_id]).first!
    daily_task.soft_delete

    render json: daily_task.as_json(with_task: true), status: :ok
  end
end
