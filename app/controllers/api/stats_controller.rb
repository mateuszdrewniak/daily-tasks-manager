# frozen_string_literal: true

class ::Api::StatsController < ::ApiController
  def show
    render json: ::Service::Stats::User.new(current_user)
  end

  def chartkick
    render json: ::Service::Stats::User.new(current_user).as_chartkick_json
  end
end
