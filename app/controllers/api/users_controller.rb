# frozen_string_literal: true

class ::Api::UsersController < ::ApiController
  def show
    render json: current_user, status: :ok
  end

  def update
    builder = ::Service::User.new(params, current_user)
    builder.update!

    sign_out(current_user) if builder.email_change

    render json: builder.to_h, status: :ok
  end
end
