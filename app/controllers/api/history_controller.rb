# frozen_string_literal: true

class ::Api::HistoryController < ::ApiController
  def month
    @month = ::Service::Calendar::Month.new(current_user, ::Time.zone.now).call

    render json: @month
  end

  def three_days
    @three_days = ::Service::Calendar::ThreeDays.new(current_user, ::Time.zone.now).call

    render json: @three_days
  end
end
