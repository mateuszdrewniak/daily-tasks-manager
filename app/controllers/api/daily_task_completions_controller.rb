# frozen_string_literal: true

class ::Api::DailyTaskCompletionsController < ::ApiController
  def update
    completion_attributes = update_completion_attributes
    service = ::Service::DailyTaskCompletion.new(completion_attributes, params[:daily_task_id], current_user)
    subject = service.create_or_update

    response_code = service.created ? :created : :ok
    render json: subject, status: response_code
  end

  private

  def update_completion_attributes
    return params.to_unsafe_h.symbolize_keys if request.format.json?
    return {} unless cookies["card-time-#{params[:daily_task_id]}"]

    ::JSON.parse(cookies["card-time-#{params[:daily_task_id]}"]).symbolize_keys
  end
end
