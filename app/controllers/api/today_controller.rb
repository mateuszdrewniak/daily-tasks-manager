# frozen_string_literal: true

class ::Api::TodayController < ::ApiController
  def show
    @todo_tasks, @done_tasks = ::Service::TasksToday.new(current_user, date_today).lists

    respond_to do |format|
      format.html { render 'today/tasks_wrapper', layout: false }
      format.json { render json: { todo_tasks: @todo_tasks.map(&:as_unified_json), done_tasks: @done_tasks.map(&:as_unified_json) } }
    end
  end

  private

  def date_today
    return ::Date.parse(params[:date]) if params[:date]

    current_user.local_time.to_date
  end
end
