# frozen_string_literal: true

class ::Api::SessionsController < ::Devise::SessionsController
  skip_before_action :verify_authenticity_token, only: %i[create destroy]
end
