# frozen_string_literal: true

class ::Api::ScheduleController < ::ApiController
  def show
    @priorities = []

    3.times do |i|
      @priorities << ::Task.active.where(user: current_user, priority: i).order('created_at DESC')
    end

    @daily_tasks = []
    7.times do |i|
      @daily_tasks << ::DailyTask.with_task.active.where(user: current_user, day: i).order('tasks.priority DESC, tasks.name ASC')
    end

    response = { priorities: @priorities, days: @daily_tasks.map { |el| el.as_json(with_task: true) } }

    render json: response
  end
end
