# frozen_string_literal: true

class ApplicationController < ::ActionController::Base
  before_action :authenticate_admin!
  around_action :set_user_timezone
  respond_to :html, :json

  def after_sign_out_path_for(*_)
    sign_in_path
  end

  protected

  def access_denied(exception)
    redirect_to root_path, alert: exception.message
  end

  def set_user_timezone(&block)
    yield && return unless current_user

    update_user_timezone unless current_user.blocked_timezone?
    yield && return unless current_user.timezone.present?

    ::Time.use_zone(current_user.timezone, &block)
  end

  def authenticate_admin!
    authenticate_user!
    return unless current_user

    current_user.admin?
  end

  def update_user_timezone
    return if (cookies['timezone'].nil? || current_user.timezone == cookies['timezone']) && (params[:timezone].nil? || current_user.timezone == params[:timezone])

    current_user.timezone = cookies['timezone'] || params[:timezone]
    current_user.save
  end
end
