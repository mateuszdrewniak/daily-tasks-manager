# frozen_string_literal: true

class ScheduleController < ApplicationController
  before_action :set_active

  def show
    @tasks = []

    3.times do |i|
      @tasks[i] = ::Task.active.where(user: current_user, priority: i).order('created_at DESC')
    end

    @daily_tasks = []
    7.times do |i|
      @daily_tasks[i] = ::DailyTask.active.where(user: current_user, day: i)
    end
  end

  private

  def set_test_data
    simulator = ::Support::Simulation::Schedule.new
    @tasks = []
    @daily_tasks = []

    3.times do |i|
      @tasks << simulator.generate_priority_tasks(priority: i)
    end

    7.times do
      @daily_tasks << simulator.generate_daily_tasks
    end
  end

  def set_active
    @active = { schedule: 'active' }
  end
end
