# frozen_string_literal: true

class ApiController < ::ApplicationController
  rescue_from ::ActiveRecord::RecordNotFound, with: :not_found
  rescue_from ::ActiveRecord::RecordNotSaved, with: :record_invalid
  rescue_from ::ActiveRecord::RecordInvalid, with: :record_invalid
  rescue_from ::ActionController::InvalidAuthenticityToken, with: :unauthorized

  skip_before_action :verify_authenticity_token

  protected

  def not_found
    head(:not_found) && return
  end

  def record_invalid(exception)
    json = { message: exception.message, class: exception.class.to_s }
    json[:errors] = exception.record&.errors if exception.is_a? ::ActiveRecord::RecordInvalid
    render json: json, status: 422
  end

  def unauthorized
    render json: { error: 'Unauthorized' }, status: :unauthorized
  end
end
