# frozen_string_literal: true

class HistoryController < ApplicationController
  include ::DateHelper

  before_action :set_active

  def month
    @month = ::Service::Calendar::Month.new(current_user, ::Time.zone.now).call
  end

  def three_days
    @three_days = ::Service::Calendar::ThreeDays.new(current_user, ::Time.zone.now).call
  end

  private

  def set_active
    @active = { history: 'active' }
  end
end
