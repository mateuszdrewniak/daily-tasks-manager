# frozen_string_literal: true

class StatsController < ApplicationController
  before_action :set_active, :set_hidden_charts

  def show
    @stats = ::Service::Stats::User.new(current_user)
    cookies['hours_day_of_week']
  end

  private

  def set_hidden_charts
    @hidden_charts = {
      day_of_week: {
        hours: '',
        minutes: ''
      },
      last_week: {
        hours: '',
        minutes: ''
      },
      this_week: {
        hours: '',
        minutes: ''
      }
    }

    @hours_in_chart = {
      day_of_week: cookies['hours_day_of_week'].inquiry.true?,
      this_week: cookies['hours_this_week'].inquiry.true?,
      last_week: cookies['hours_last_week'].inquiry.true?
    }

    type = cookies['hours_day_of_week'].inquiry.true? ? :minutes : :hours
    @hidden_charts[:day_of_week][type] = 'hidden'

    type = cookies['hours_this_week'].inquiry.true? ? :minutes : :hours
    @hidden_charts[:this_week][type] = 'hidden'

    type = cookies['hours_last_week'].inquiry.true? ? :minutes : :hours
    @hidden_charts[:last_week][type] = 'hidden'
  end

  def set_active
    @active = { stats: 'active' }
  end
end
