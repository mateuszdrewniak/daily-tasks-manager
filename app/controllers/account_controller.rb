# frozen_string_literal: true

class AccountController < ApplicationController
  include ::SymbolHelper

  before_action :set_active

  def show; end

  private

  def set_active
    @active = { my_account: 'active' }
  end
end
