# frozen_string_literal: true

class TodayController < ::ApplicationController
  include ::DateHelper

  before_action :set_active

  def show; end

  private

  def set_active
    @active = { today: 'active' }
  end
end
