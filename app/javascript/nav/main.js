$(document).ready( () => {

    function hideInfo() {
        if($('#info p').text() != ''){
            $('#info').animate({top: '+=100'}, 0, 'swing')
            setTimeout( () => {
                $('#info').animate({top: '-=100'}, 500, 'swing')
                setTimeout( () => {
                    $('#info').hide()
                }, 500)
            }, 2500)
        }
    }

    $(window).on('load', hideInfo)
})