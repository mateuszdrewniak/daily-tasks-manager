export default class SymbolHandler {
  static prioritySymbolClass(priority) {
    switch(priority){
      case 1:
        return 'priority-symbol fas fa-angle-up'
      case 2:
        return 'priority-symbol fas fa-angle-double-up'
      default:
        return 'priority-symbol fas fa-minus'
    }
  }
}