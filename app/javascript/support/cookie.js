export default class Cookie {
  static set(name, value, exdays, path = '/') {
    let date = new Date()
    date.setTime(date.getTime() + (exdays * 24 * 60 * 60 * 1000))
    let expires = "expires="+ date.toUTCString()
    document.cookie = name + "=" + value + ";" + expires + ";path=" + path
  }

  static get(name) {
    let value = " " + document.cookie
    let start = value.indexOf(" " + name + "=")

    if (start == -1) {
      value = null
    }
    else {
      start = value.indexOf("=", start) + 1
      let end = value.indexOf(";", start)
      if (end == -1) {
          end = value.length
      }
      value = unescape(value.substring(start, end))
    }

    return value
  }
}
