export default class PercentHandler {
  static updatePercent() {
    let toDoNumber = $("#to-do .card").length
    let doneNumber = $("#done .card").length
    if(toDoNumber == null || doneNumber == null) {
      $("#done-percent").text("0%")
      this.updateColourClass(percent)
      return
    }

    let percent = (doneNumber / (toDoNumber + doneNumber)) * 100
    if(Math.floor(percent) != percent) percent = percent.toFixed(1)
    
    $("#done-percent").text(percent.toString() + "%")
    this.updateColourClass(percent)
  }

  static updateColourClass(percent) {
    this.clearColourClass()
    $("#done-percent").addClass(this.colourClass(percent))
  }

  static clearColourClass() {
    ["progress-0", "progress-25", "progress-50", "progress-75", "progress-100"].forEach(
      el => $("#done-percent").removeClass(el)
    )
  }

  static colourClass(percent) {
    let colourClass

    if(percent < 25) {
      colourClass = "progress-0"
    }
    else if(percent < 50) {
      colourClass = "progress-25"
    }
    else if(percent < 75) {
      colourClass = "progress-50"
    }
    else if(percent < 100) {
      colourClass = "progress-75"
    }
    else if(percent == 100) {
      colourClass = "progress-100"
    }

    return colourClass
  }
}