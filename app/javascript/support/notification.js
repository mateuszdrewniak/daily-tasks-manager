export default class Notification {
  static displayNotice(message) {
    $('#info').addClass('notice')
    $('#info p').text(message)

    $('#info').animate({top: '+=100'}, 0, 'swing')
    $('#info').show()
    setTimeout( () => {
      $('#info').animate({top: '-=100'}, 500, 'swing')
      setTimeout( () => {
          $('#info').hide()
      }, 500)
    }, 1500)
  }

  static displayAlert(message) {
    $('#info').addClass('alert')
    $('#info p').text(message)

    $('#info').animate({top: '+=100'}, 0, 'swing')
    $('#info').show()
    setTimeout( () => {
      $('#info').animate({top: '-=100'}, 500, 'swing')
      setTimeout( () => {
          $('#info').hide()
      }, 500)
    }, 1500)
  }

  static formError(message) {
    return "<p class='form-error'>" + message + "</p>"
  }

  static cleanFormErrors(parentClass = '') {
    if(parentClass != '')
      parentClass += ' '
    $(`${parentClass}.form-error`).remove()
    $(`${parentClass}.input`).removeClass('with-error')
  }

  static showFormErrors(data, modelName, parentClass = '') {
    let input, message
    if(parentClass != '')
      parentClass += ' '

    if(data != null && data != '' && data['errors'] != null) {
      for(let key in data['errors']) {
        input = $(`${parentClass}.input.${modelName}_${key}`)
        input.addClass('with-error')
        data['errors'][key].forEach((error) => {
          message = this.formError(error)
          input.append(message)
        })
      }
    } else {
      this.displayAlert('An error occured')
    }
  }
}