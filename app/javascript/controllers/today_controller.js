import { Controller } from "stimulus"
import PercentHandler from "../support/percent-handler.js"
import Notification from "../support/notification.js"
import DateFormat from "../support/date-format.js"

export default class extends Controller {
  connect() {
    this.loadCards()
  }

  loadCards() {
    let url = "/api/today/" + DateFormat.today()
    let xhr = new XMLHttpRequest()

    xhr.open("GET", url, true)
    xhr.setRequestHeader("Content-Type", "application/json")
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4) {
        if(xhr.status === 200 || xhr.status === 201) {
          // on ready state change
          $(".tasks-wrapper").remove()
          $("main").append(xhr.response)
          PercentHandler.updatePercent()
        } else {
          Notification.displayAlert('An error occurred')
        }
      }
    }
    xhr.send()
  }
}
