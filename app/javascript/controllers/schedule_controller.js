import { Controller } from "stimulus"
import SymbolHandler from "../support/symbol-handler.js"
import Notification from "../support/notification.js"

export default class extends Controller {
  currentPriority = 0
  currentDay = 0
  draggedId = -1
  draggedType = null
  draggedTaskPriority = -1
  preventDeletion = false
  preventTaskDrop = false

  // navigation

  leftPriority(e) {
    let previousPriority = this.currentPriority
    this.currentPriority -= 1
    if(this.currentPriority == -1) this.currentPriority = 2

    $("#priority-" + previousPriority).addClass("hidden-schedule")
    $("#priority-" + previousPriority).removeClass("visible-schedule")

    $("#priority-" + this.currentPriority).addClass("visible-schedule")
    $("#priority-" + this.currentPriority).removeClass("hidden-schedule")
  }

  rightPriority(e) {
    let previousPriority = this.currentPriority
    this.currentPriority += 1
    if(this.currentPriority == 3) this.currentPriority = 0
    
    $("#priority-" + previousPriority).addClass("hidden-schedule")
    $("#priority-" + previousPriority).removeClass("visible-schedule")

    $("#priority-" + this.currentPriority).addClass("visible-schedule")
    $("#priority-" + this.currentPriority).removeClass("hidden-schedule")
  }

  leftDay(e) {
    let previousDay = this.currentDay
    this.currentDay -= 1
    if(this.currentDay == -1) this.currentDay = 6

    $("#day-" + previousDay).addClass("hidden-schedule")
    $("#day-" + previousDay).removeClass("visible-schedule")

    $("#day-" + this.currentDay).addClass("visible-schedule")
    $("#day-" + this.currentDay).removeClass("hidden-schedule")
  }

  rightDay(e) {
    let previousDay = this.currentDay
    this.currentDay += 1
    if(this.currentDay == 7) this.currentDay = 0
    
    $("#day-" + previousDay).addClass("hidden-schedule")
    $("#day-" + previousDay).removeClass("visible-schedule")

    $("#day-" + this.currentDay).addClass("visible-schedule")
    $("#day-" + this.currentDay).removeClass("hidden-schedule")
  }

  addCardIsPresented() {
    return this.addCardAfter != -1
  }

  // Modals

  openCreateTaskModal(e) {
    let priority = e.currentTarget.dataset.priority
    $('#create-task-modal').show(50)
    $('#create-task-modal select').val(priority)
  }

  closeCreateTaskModal() {
    $('#create-task-modal').hide(50)
  }

  openEditTaskModal(e) {
    let priority = e.currentTarget.dataset.priority
    let taskId = e.currentTarget.dataset.taskId
    let name = $('#task-' + taskId + ' .name').text()
    let time = $('#task-' + taskId + ' .goal').text()

    $('#edit-task-modal #name').val(name)
    $('#edit-task-modal #id').val(taskId)
    $('#edit-task-modal select').val(priority)
    $('#edit-task-modal #time').val(time)
    $('#edit-task-modal').show(50)
  }

  closeEditTaskModal() {
    $('#edit-task-modal').hide(50)
  }

  deleteTask(e) {
    let id = $('#edit-task-modal #id').val().toString()
    let url = "/api/tasks/" + id
    let xhr = new XMLHttpRequest()

    xhr.open("DELETE", url, true)
    xhr.setRequestHeader("Content-Type", "application/json")
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4) {
        if(xhr.status === 200 || xhr.status === 201) {
          // on ready state change
          console.log(xhr.response)
          let response = JSON.parse(xhr.response)
          $('.task-' + response.id.toString()).remove()
          $('#edit-task-modal').hide(50)
          Notification.displayNotice('Task "' + response.name + '" deleted')
        } else {
          console.log(xhr.response)
        }
      }
    }
    xhr.send()
  }

  taskCreated(e) {
    let data, status, xhr
    [data, status, xhr] = e.detail
    let task = $('#new-task-container .card').clone()
    $('#priority-' + data.priority.toString() + ' .card-body').prepend(task)
    let newCard = $('#priority-' + data.priority.toString() + ' .card-body .card').first()
    newCard.attr("data-task-id", data.id)
    newCard.attr("data-priority", data.priority)
    newCard.prop("id", 'task-' + data.id.toString())
    newCard.addClass('task-' + data.id.toString())
    $('#task-' + data.id.toString() + ' .name').text(data.name)
    $('#task-' + data.id.toString() + ' .goal').text(data.time)
    $('#task-' + data.id.toString() + ' .right').html($('#priority-example-' + data.priority.toString() + ' span').first())

    Notification.cleanFormErrors('#create-task-modal')

    this.closeCreateTaskModal()
    Notification.displayNotice('Task "' + data.name + '" created')
  }

  taskNotCreated(e) {
    let data, status, xhr
    [data, status, xhr] = e.detail
    
    Notification.cleanFormErrors('#create-task-modal')
    Notification.showFormErrors(data, 'task', '#create-task-modal')
  }

  taskEdited(e) {
    let data, status, xhr
    [data, status, xhr] = e.detail

    Turbolinks.visit('/schedule')
  }

  taskNotEdited(e) {
    let data, status, xhr
    [data, status, xhr] = e.detail
    console.log(data)

    Notification.cleanFormErrors('#edit-task-modal')
    Notification.showFormErrors(data, 'task', '#edit-task-modal')
  }

  // Drag and drop

  taskDragStart(e) {
    this.draggedType = 'task'
    let cardId = e.currentTarget.id
    e.dataTransfer.dropEffect = "copy"
    this.draggedId = cardId
    this.draggedTaskPriority = e.currentTarget.dataset.priority
    $("#" + cardId).addClass("dragged")
  }

  taskDragEnd(e) {
    this.draggedId = -1
    this.draggedTaskPriority = -1
    this.draggedType = null
    let cardId = e.currentTarget.id
    $("#" + cardId).removeClass("dragged")
  }

  dragStart(e) {
    this.draggedType = 'task'
    let cardId = e.currentTarget.id
    this.draggedId = cardId
    $("#" + cardId).addClass("dragged")
  }

  dayDragEnter(e) {
    e.stopPropagation()
    if(this.draggedType == "task-per-day") return

    let dayId = "day-" + e.currentTarget.dataset.dayId
    $("#" + dayId + ' .card-group').addClass("dragged-over")

    $("#" + dayId + ' .card').toArray().forEach(el => {
      if(("task-" + el.dataset.taskId) == this.draggedId) {
        this.showBanSymbol(dayId)
        return
      }
    })

    if(this.preventTaskDrop) return

    let addCardCopy = $("#add-card-container div.add-card").clone()
    $("#" + dayId + " .priority-container-" + this.draggedTaskPriority.toString()).prepend(addCardCopy)
    $(".card-group .add-card").show()
    $("#" + dayId + " .day-card-plus-symbol").removeClass("hidden")
  }

  showBanSymbol(dayId) {
    $("#" + dayId + ' .day-card-ban-symbol').removeClass("hidden")
    this.preventTaskDrop = true
  }

  dayDragLeave(e) {
    e.stopPropagation()
    if(this.draggedType == "task-per-day") return

    let dayId = "day-" + e.currentTarget.dataset.dayId
    $("#" + dayId + " .card-group").removeClass("dragged-over")
    $(".card-group .add-card").remove()
    $("#" + dayId + " .day-card-plus-symbol").addClass("hidden")
    $("#" + dayId + ' .day-card-ban-symbol').addClass("hidden")
    this.preventTaskDrop = false
  }

  dayDragDrop(e) {
    e.stopPropagation()
    if(this.draggedType == "task-per-day") return

    let dayId = "day-" + e.currentTarget.dataset.dayId
    if(this.preventTaskDrop) {
      $("#" + dayId + ' .day-card-ban-symbol').addClass("hidden")
      this.preventTaskDrop = false
      return
    }

    let taskDbId = Number(this.draggedId.split('-')[1])
    let dayDbId = Number(dayId.split('-')[1])

    $(".card-group .add-card").remove()
    $("#" + dayId + " .day-card-plus-symbol").addClass("hidden")
    $("#" + dayId + " .card-group").removeClass("dragged-over")
    $("#" + this.draggedId).removeClass("dragged")

    var draggedCardCopy = $("#" + this.draggedId).clone()
    var draggedId = this.draggedId
    var draggedTaskPriority = this.draggedTaskPriority

    // save to DB
    let url = "/api/daily_tasks"
    let xhr = new XMLHttpRequest()

    let request = {
      'task_id': taskDbId,
      'day': dayDbId
    }

    var dailyTaskCreateCallback = function(response) {
      $("#" + dayId + " .priority-container-" + draggedTaskPriority.toString()).prepend(draggedCardCopy)
      let newCard = $(".days-wrapper #" + draggedId)
      newCard.prepend($("#delete-card-container .delete-card").clone())
      $(".days-wrapper #" + draggedId + " .delete-card").attr("data-id", response.id)
      newCard.prop("id", "task-per-day-" + response.id)
      newCard.attr('data-task-per-day-id', response.id)
      newCard.attr("data-action", "dragstart->schedule#dailyTaskDragStart dragend->schedule#dailyTaskDragEnd")
    }

    xhr.open("POST", url, true)
    xhr.setRequestHeader("Content-Type", "application/json")
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4) {
        if(xhr.status === 200 || xhr.status === 201) {
          // on ready state change
          console.log(xhr.response)
          let response = JSON.parse(xhr.response)
          dailyTaskCreateCallback(response)
        } else {
          Notification.displayAlert('An error occured')
          console.log(xhr.response)
        }
      }
    }
    xhr.send(JSON.stringify(request))

    this.draggedId = -1
    this.draggedTaskPriority = -1
    this.draggedType = null
  }

  dailyTaskDragStart(e) {
    this.draggedType = 'task-per-day'
    let cardId = e.currentTarget.id
    this.draggedId = cardId
    setTimeout( () => {
      $("#" + cardId + " .delete-card").show()
    }, 1)
  }

  dailyTaskDragEnter(e) {
    e.stopPropagation()
    if(this.draggedType == "task-per-day" || this.addCardIsPresented()) return

    let cardId = e.currentTarget.id
    this.addCardAfter = cardId
    $("#" + cardId).addClass("dragged-over")
    let addCardCopy = $("#add-card-container div.add-card").clone()
    $("#" + cardId).after(addCardCopy)
    $(".card-group .add-card").show()
  }

  dailyTaskDragLeave(e) {
    e.stopPropagation()
    if(this.draggedType == "task-per-day" || !this.addCardIsPresented()) return

    this.addCardAfter = -1
    let cardId = e.currentTarget.id
    $("#" + cardId).removeClass("dragged-over")
    $(".card-group .add-card").remove()
  }

  dailyTaskDragEnd(e) {
    let cardId = e.currentTarget.id
    this.draggedId = -1
    this.draggedType = null

    if(this.preventDeletion) {
      this.preventDeletion = false
      $("#" + cardId + " .delete-card").hide("scale")
      return
    }

    let nameArray = cardId.split('-')
    let dailyTaskId = nameArray[nameArray.length - 1]
    this.deleteDailyTask(dailyTaskId)
  }

  deleteDailyTask(id) {
    let url = "/api/daily_tasks/" + id
    let xhr = new XMLHttpRequest()

    xhr.open("DELETE", url, true)
    xhr.setRequestHeader("Content-Type", "application/json")
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4) {
        if(xhr.status === 200 || xhr.status === 201) {
          // on ready state change
          console.log(xhr.response)
          $("#task-per-day-" + id).hide("drop", 200)
          setTimeout( () => {
            $("#task-per-day-" + id).remove()
          }, 300)
        } else {
          console.log(xhr.response)
          Notification.displayAlert('An error occured')
          $("#task-per-day-" + id + ' .delete-card').hide("scale")
        }
      }
    }
    xhr.send()
  }

  defaultDragOver(e) {
    e.preventDefault()
  }

  deleteCardDragDrop(e) {
    this.preventDeletion = true
  }
}
