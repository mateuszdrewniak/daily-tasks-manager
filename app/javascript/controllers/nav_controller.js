import { Controller } from "stimulus"

export default class extends Controller {
  static targets = []
  
  openSmallNav() {
    $('#nav-list').show('slow')
    $('#burger-close-icon').show()
    $('#burger-icon').hide()
    $('#nav-active-overlay').show()
  }

  closeSmallNav() {
    $('#nav-list').hide('slow')
    $('#burger-close-icon').hide()
    $('#burger-icon').show()
    $('#nav-active-overlay').hide()
  }

  openMediumNav() {
    $('#md-nav-list').show().animate({left: '+=' + $('#md-nav-list').outerWidth()}, 458, 'swing')
    $('#md-nav-active-overlay').show().animate({opacity: '0.6'}, 458, 'swing')
  }

  closeMediumNav(time) {
    if(!Number.isInteger(time)) time = 458
    $('#md-nav-list').animate({left: '-=' + $('#md-nav-list').outerWidth()}, time, 'swing')
    $('#md-nav-active-overlay').animate({opacity: '0'}, time, 'swing')
    setTimeout(() => {
        $('#md-nav-list').hide()
        $('#md-nav-active-overlay').hide()
    }, time)
  }

  visitLinkMedium(e) {
    this.closeMediumNav(250)
    var href = e.currentTarget.href
    setTimeout(() => {
      Turbolinks.visit(href)
    }, 250)
    e.preventDefault()
  }

  visitLinkSmall(e) {
    this.closeSmallNav()
    var href = e.currentTarget.href
    setTimeout(() => {
      Turbolinks.visit(href)
    }, 520)
    e.preventDefault()
  }
}
