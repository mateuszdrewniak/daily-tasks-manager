import { Controller } from "stimulus"
import Notification from "../support/notification.js"

export default class extends Controller {
  connect() {
    $("#user_auto_timezone").on('change', this.toggleTimezoneEvent)
  }

  toggleTimezoneEvent(e) {
    if(e.currentTarget.checked) 
      $("#user_timezone").prop("disabled", true)
    else
      $("#user_timezone").prop("disabled", false)
  }

  userUpdated(e) {
    let data, status, xhr
    [data, status, xhr] = e.detail
    
    Notification.cleanFormErrors()

    if(data['email_change']) {
      Notification.displayNotice('Confirmation instructions have been sent to your new email')
      setTimeout(() => {
        Turbolinks.visit('/my_account?email_change=true')
      }, 2000)
      return
    }

    Notification.displayNotice('Account updated')
    setTimeout(() => {
      Turbolinks.visit('/my_account')
    }, 2000)
  }

  userNotUpdated(e) {
    let data, status, xhr
    [data, status, xhr] = e.detail

    Notification.cleanFormErrors()
    Notification.showFormErrors(data, 'user')
  }
}
