import { Controller } from "stimulus"
import PercentHandler from "../support/percent-handler.js"

export default class extends Controller {
  connect() {
    PercentHandler.updatePercent()
  }
}
