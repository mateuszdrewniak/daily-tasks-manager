import { Controller } from "stimulus"
import PercentHandler from "../support/percent-handler.js"
import Cookie from "../support/cookie.js"
import DateFormat from "../support/date-format.js"
import Notification from "../support/notification.js"

export default class extends Controller {
  static targets = [ "timer" ]
  currentInterval = -1
  timeInSeconds = -1
  dayCheckInterval = -1
  currentDate = null
  currentCard = null

  disconnect() {
    clearInterval(this.currentInterval)
    clearInterval(this.dayCheckInterval)
    if(this.currentCard) this.persistSpentTime(this.currentCard)
  }

  connect() {
    this.currentDate = DateFormat.today()
    this.dayCheckInterval = setInterval( () => {
      this.reloadIfNextDay()
    }, 60000)
  }

  start(e) {
    let currentCard = e.currentTarget.dataset.stopwatchCard
    this.startCard(currentCard)
  }

  stop(e) {
    let currentCard = e.currentTarget.dataset.stopwatchCard
    this.stopCard(currentCard)
  }

  reloadIfNextDay() {
    let today = DateFormat.today()
    if(this.currentDate == null || this.currentDate == today) return

    this.currentDate = today
    window.location.reload()
  }

  measureTime(active) {
    let timeString = $("#timer-" + active).text()
    if(timeString.length < 8) timeString = '00:00:00'
    let timeArray = timeString.split(':')
    timeArray = timeArray.map(Number)
    let date = new Date()
    let currentTimeInSeconds = parseInt(date.getTime() / 1000)
    
    let difference = this.timeInSeconds == -1 ? 1 : currentTimeInSeconds - this.timeInSeconds

    timeArray[2] += difference
    if(timeArray[2] >= 60){
      timeArray[2] = timeArray[2] - 60
      timeArray[1] += 1
      if(timeArray[1] == 60){
        timeArray[1] = 0
        timeArray[0] += 1
        if(timeArray[0] == 24){
          this.blockCard(active)
          return false
        }
      }
    }

    let current = timeArray[2] + (timeArray[1] * 60) + (timeArray[0] * 3600)

    for(let i = 0; i < 3; i++) if(timeArray[i] < 10) timeArray[i] = '0' + timeArray[i]

    let time = timeArray.join(':')
    let cookieBody = {
      spent_time: time,
      date: DateFormat.today(),
      completed: false
    }

    $("#timer-" + active).text(time)
    if($('#to-do #timer-' + active).length == 0){
      time += '+'
      cookieBody['completed'] = true
    }
    document.title = time
    Cookie.set("card-time-" + active, JSON.stringify(cookieBody), 1)

    let goalArray = $("#goal-" + active).text().split(':')
    goalArray = goalArray.map(Number)
    let goal = goalArray[2] + (goalArray[1] * 60) + (goalArray[0] * 3600)

    let percent = (current / goal) * 100
    $("#task-" + active + ' .progress-bar').width(percent + '%')
    
    if(percent == 100) {
      if(cookieBody['completed']) {
        this.stopCard(active)
        $('#start-' + active).remove()
      }
      else this.finishCard(active)
    }

    this.timeInSeconds = currentTimeInSeconds
  }

  startCard(card) {
    $("#start-" + card).hide()
    $("#stop-" + card).show()

    $("#timer-" + card).addClass('text-spec')
    $("#plus-" + card).addClass('text-spec')
    $("#task-" + card + ' .progress-bar').addClass('bg-spec')

    if(this.data.get('activeCard') != '-1')
      this.stopCard(this.data.get('activeCard'))

    this.data.set('activeCard', card)

    this.currentCard = card
    this.currentInterval = setInterval( () => {
      this.measureTime(parseInt(this.data.get('activeCard')))
    }, 1000)
  }

  stopCard(card, finish = false) {
    $("#stop-" + card).hide()
    $("#start-" + card).show()

    $("#timer-" + card).removeClass('text-spec')
    $("#plus-" + card).removeClass('text-spec')
    $("#task-" + card + ' .progress-bar').removeClass('bg-spec')

    this.data.set('activeCard', '-1')

    if(this.currentInterval != -1)
      clearInterval(this.currentInterval)
    
    let title = 'Today'
    
    if($('#to-do .card').length == 0) title += ' - All Done'

    title += ' | Daily Manager'
    
    document.title = title
    
    this.currentInterval = -1
    this.timeInSeconds = -1
    this.currentCard = null

    if(!finish) this.persistSpentTime(card)
  }

  persistSpentTime(card) {
    let url = "/api/daily_task_completions/" + card
    let xhr = new XMLHttpRequest()

    xhr.open("PUT", url, true)
    xhr.setRequestHeader("Content-Type", "application/json")
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4) {
        if(xhr.status === 200 || xhr.status === 201) {
          // on ready state change
        } else {
          Notification.displayAlert('An error occured')
        }
      }
    }
    xhr.send()
  }

  finishCard(card) {
    this.stopCard(card, true)
    let currentCard = $("#task-" + card)
    $("#timer-" + card).addClass('text-green')
    $("#task-" + card + ' .progress-bar').remove()
    $("#task-" + card + ' .black-bottom').append('<span id="plus-' + card + '" class="text-green">+</span>')
    currentCard.hide( "slide", { direction: "right" }, 300 )

    $("#start-" + card).addClass('text-main border-main border-main-hoverable')
    $("#start-" + card).removeClass('bg-main-hoverable text-light shadow')

    setTimeout( () => {
      if($('#to-do .card').length == 1) { 
        $('#nothing-to-do').show("slide", { direction: "up" }, 300)
        setTimeout( () => {
          let allDone = new Audio('sounds/error-sound-effect.mp3')
          allDone.play()
        }, 1000)
      }
      if($('#done .card').length == 0) $('#nothing-done').hide("slide", { direction: "up" }, 300)
      $('#done .card-top').after(currentCard)
      currentCard.show( "slide", { direction: "left" }, 300 )
      this.startCard(card)
      $("#timer-" + card).addClass('text-spec')
      PercentHandler.updatePercent()
    }, 301)

    let cookieBody = {
      spent_time: $("#timer-" + card).text(),
      date: DateFormat.today(),
      completed: true
    }
    Cookie.set("card-time-" + card, JSON.stringify(cookieBody), 1)

    this.persistSpentTime(card)
    
    let taskDone = new Audio('sounds/interface-beep-up-sound-effect.mp3')
    taskDone.play()
  }

  blockCard(card) {
    this.stopCard(card)
    $("#start-" + card).hide()
  }
}
