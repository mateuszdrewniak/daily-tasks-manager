import { Controller } from "stimulus"
import Cookie from "../support/cookie.js"
import Notification from "../support/notification.js"

export default class extends Controller {
  connect() {
    let timezone = Intl.DateTimeFormat().resolvedOptions().timeZone
    let timezoneCookie = Cookie.get('timezone')

    if(timezoneCookie == null || timezoneCookie != timezone) Cookie.set('timezone', timezone, 365)
  }
}
