import { Controller } from "stimulus"
import Cookie from "../support/cookie.js"
import Notification from "../support/notification.js"

export default class extends Controller {
  connect() {
  }

  toggleChartHours(e) {
    let target = e.currentTarget
    let id = target.id.substring(6)
    let hour_charts = document.querySelectorAll(`#${id} .hours`)
    let minute_charts = document.querySelectorAll(`#${id} .minutes`)

    if(target.checked) {
      for(let chart of minute_charts) {
        chart.classList.add('hidden')
      }

      for(let chart of hour_charts) {
        chart.classList.remove('hidden')
      }
    } else {
      for(let chart of minute_charts) {
        chart.classList.remove('hidden')
      }

      for(let chart of hour_charts) {
        chart.classList.add('hidden')
      }
    }

    Chartkick.eachChart((chart) => {
      chart.redraw()
    })

    Cookie.set(target.id, target.checked, 365)
  }
}
