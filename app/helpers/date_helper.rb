# frozen_string_literal: true

module DateHelper
  def today_long
    date = ::Time.zone.now

    ::I18n.t("days.#{weekday(date)}") + ", #{date.day} " + ::I18n.t("months.#{date.month}")
  end

  def month_long
    date = ::Time.zone.now

    ::I18n.t("months.#{date.month}") + ", #{date.year}"
  end

  def weekday(date)
    ::Support::Date.weekday(date)
  end

  def to_iso(date)
    ::Support::Date.to_iso(date)
  end

  def today
    ::Support::Date.today
  end
end
