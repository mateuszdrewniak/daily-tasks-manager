# frozen_string_literal: true

module HistoryHelper
  def day_orb_bg(active = false)
    if active
      'bg-spec'
    elsif active.nil?
      'hidden'
    else
      'bg-main'
    end
  end

  def progress_bar_bg(completed = false, current = false)
    if completed
      "bg-green"
    elsif current
      "bg-spec"
    else
      "bg-main"
    end
  end

  def task_pill_bg(completed = false, current = false)
    if completed
      "bg-light-green"
    elsif current
      "bg-light-spec"
    else
      "bg-light-main"
    end
  end

  def day_tile_bg(active = false, weekend = false)
    if active
      'bg-light-group'
    elsif active.nil?
      'bg-gray-200'
    elsif weekend
      'bg-lighter-warning'
    end
  end
end
