# frozen_string_literal: true

module SymbolHelper
  def priority_symbol(priority:, centered: false)
    color, symbol = priority_classes(priority, centered)

    haml_tag :span, class: "priority #{color}" do
      haml_tag :i, class: symbol
    end
  end

  def trash_symbol
    haml_tag :i, class: trash_class
  end

  def plus_symbol
    haml_tag :i, class: plus_class
  end

  def ban_symbol
    haml_tag :i, class: ban_class
  end

  def level_symbol(level)
    class_h = level_class(level)

    haml_tag :div, class: "level-block #{class_h[:background_color][:class]}", title: 'Level | Streak' do
      haml_tag :i, class: "fas fa-#{class_h[:icon]} #{class_h[:color][:class]}"
      haml_tag :span, class: class_h[:color][:class] do
        haml_concat level
      end
    end
  end

  # Module functions

  def priority_classes(priority, centered)
    color = centered ? 'center-block ' : ''
    case priority
    when 1
      color += 'text-priority-1'
      symbol = 'priority-symbol fas fa-angle-up'
    when 2
      color += 'text-priority-2'
      symbol = 'priority-symbol fas fa-angle-double-up'
    else
      color += 'text-priority-0'
      symbol = 'priority-symbol fas fa-minus'
    end

    [color, symbol]
  end

  def trash_class
    'fas fa-trash'
  end

  def plus_class
    'fas fa-plus'
  end

  def ban_class
    'fas fa-ban'
  end

  def level_class(level)
    if level > 59
      { icon: 'dragon', color: ::Support::Colors::TEXT_COLOR[:orange], background_color: ::Support::Colors::BACKGROUND_COLOR[:black] }
    elsif level > 51
      { icon: 'gem', color: ::Support::Colors::TEXT_COLOR[:blue], background_color: ::Support::Colors::BACKGROUND_COLOR[:light] }
    elsif level > 44
      { icon: 'hat-wizard', color: ::Support::Colors::TEXT_COLOR[:wizard_green], background_color: ::Support::Colors::BACKGROUND_COLOR[:black] }
    elsif level > 36
      { icon: 'sun', color: ::Support::Colors::TEXT_COLOR[:yellow], background_color: ::Support::Colors::BACKGROUND_COLOR[:black] }
    elsif level > 29
      { icon: 'dove', color: ::Support::Colors::TEXT_COLOR[:black], background_color: ::Support::Colors::BACKGROUND_COLOR[:light] }
    elsif level > 23
      { icon: 'fish', color: ::Support::Colors::TEXT_COLOR[:turqoise], background_color: ::Support::Colors::BACKGROUND_COLOR[:black] }
    elsif level > 19
      { icon: 'chess-knight', color: ::Support::Colors::TEXT_COLOR[:black], background_color: ::Support::Colors::BACKGROUND_COLOR[:light] }
    elsif level > 13
      { icon: 'bomb', color: ::Support::Colors::TEXT_COLOR[:black], background_color: ::Support::Colors::BACKGROUND_COLOR[:light] }
    elsif level > 10
      { icon: 'kiwi-bird', color: ::Support::Colors::TEXT_COLOR[:kiwi_green], background_color: ::Support::Colors::BACKGROUND_COLOR[:black] }
    elsif level > 6
      { icon: 'fist-raised', color: ::Support::Colors::TEXT_COLOR[:orange], background_color: ::Support::Colors::BACKGROUND_COLOR[:black] }
    elsif level > 3
      { icon: 'otter', color: ::Support::Colors::TEXT_COLOR[:green], background_color: ::Support::Colors::BACKGROUND_COLOR[:black] }
    elsif level > 2
      { icon: 'bolt', color: ::Support::Colors::TEXT_COLOR[:yellow], background_color: ::Support::Colors::BACKGROUND_COLOR[:black] }
    elsif level > 1
      { icon: 'fire-alt', color: ::Support::Colors::TEXT_COLOR[:orange], background_color: ::Support::Colors::BACKGROUND_COLOR[:black] }
    elsif level.positive?
      { icon: 'frog', color: ::Support::Colors::TEXT_COLOR[:green], background_color: ::Support::Colors::BACKGROUND_COLOR[:black] }
    else
      { icon: 'carrot', color: ::Support::Colors::TEXT_COLOR[:orange], background_color: ::Support::Colors::BACKGROUND_COLOR[:black] }
    end
  end

  module_function :level_class, :ban_class, :plus_class, :trash_class, :priority_classes
end
