# frozen_string_literal: true

module EmailHelper
  def button_styles
    "background-color: #7848c4;color: #fafbfc;text-decoration: none;text-align: center;display: block;padding: 0.3rem 0.5rem;border-radius: 0.25rem;box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06);"
  end

  def body_styles
    "padding: 20px;width: 100%;height: 100%;background-color: #edf2f7;font-family: Arial, Helvetica, sans-serif;"
  end

  def card_styles
    "width: 400px;margin-left: auto;margin-right: auto;padding: 1rem;margin-top: 0.5rem;box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06);border-radius: 0.25rem;background-color: #fafbfc;margin-bottom: 40px;"
  end
end
