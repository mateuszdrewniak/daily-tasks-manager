# frozen_string_literal: true

module ApplicationHelper
  def spinner
    haml_tag :div, class: 'spinner' do
      haml_tag :i, class: 'fas fa-circle-notch'
    end
  end

  def input_mock(label, value, enabled = true)
    enabled_class = enabled ? '' : 'disabled'

    haml_tag :div, class: 'input' do
      haml_tag :div, class: 'label-mock' do
        haml_concat label
      end
      haml_tag :div, class: "input-mock #{enabled_class}" do
        haml_concat value
      end
    end
  end
end
