# frozen_string_literal: true

module AdminHelper
  def admin_priority_symbol(priority:, centered: false)
    color, symbol = ::SymbolHelper.priority_classes(priority, centered)

    span class: "priority #{color}" do
      i class: symbol
    end
  end

  def admin_trash_symbol
    i class: ::SymbolHelper.trash_class
  end

  def admin_plus_symbol
    i class: ::SymbolHelper.plus_class
  end

  def admin_ban_symbol
    i class: ::SymbolHelper.ban_class
  end

  def admin_level_symbol(level)
    class_h = ::SymbolHelper.level_class(level)

    haml_tag :div, class: "level-block #{class_h[:background_color][:class]}", title: 'Level | Streak' do
      haml_tag :i, class: "fas fa-#{class_h[:icon]} #{class_h[:color][:class]}"
      haml_tag :span, class: class_h[:color][:class] do
        haml_concat level
      end
    end
  end
end
