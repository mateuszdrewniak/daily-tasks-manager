# frozen_string_literal: true

class ::Null::User < ::Null
  def level
    0
  end

  def friendly_timezone
    'none'
  end

  def local_time
    ::Time.current
  end

  def blocked_timezone?
    true
  end

  def auto_timezone
    false
  end

  def avatar_source
    'none'.inquiry
  end

  def avatar_url
    gravatar_url
  end

  def gravatar_url
    'none'
  end

  def account_type
    'none'.inquiry
  end

  def display_name; end

  alias name display_name

  def email_name; end
end
