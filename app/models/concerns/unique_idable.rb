# frozen_string_literal: true

module UniqueIdable
  def generate_unique_id(column:, prefix:, length: 10, separator: '_', klass: self.class)
    UniqueId.new(klass: klass, column: column, prefix: prefix, length: length, separator: separator)
  end

  class UniqueId
    attr_reader :klass, :column, :prefix, :length, :separator, :timestamp, :value

    def initialize(klass:, column:, prefix:, length: 10, separator: '_')
      @klass = klass
      @column = column.to_sym
      @prefix = prefix.to_s
      @length = length
      @separator = separator
      @timestamp = ::Time.current.to_i
      @value = generate_unique
    end

    def to_s
      value
    end

    private

    def timestamp_hex
      timestamp.to_s(16)
    end

    def generate_unique
      loop do
        data = ::SecureRandom.urlsafe_base64(length)
        data.tr('_', '')
        id = "#{prefix}#{separator}#{timestamp_hex}#{data}"
        break id unless klass.where(column => id).first
      end
    end
  end
end
