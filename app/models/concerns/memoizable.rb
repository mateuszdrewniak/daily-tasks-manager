# frozen_string_literal: true

module Memoizable
  def memoize(*method_names)
    method_names = method_names.first if method_names.first.is_a?(::Array)
    method_names.each do |method_name|
      raise ::ArgumentError, 'Incorrect memoize paramer!' unless method_name.is_a?(::Symbol)

      define_method method_name do
        var_value = instance_variable_get(:"@#{method_name}")
        return var_value if var_value.present?

        instance_variable_set(:"@#{method_name}", __send__(:"__#{method_name}__"))
      end
    end
  end
end
