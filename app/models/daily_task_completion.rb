# frozen_string_literal: true

class DailyTaskCompletion < ApplicationRecord
  belongs_to :daily_task
  belongs_to :user

  scope :for_user, lambda { |u|
    user_id = u.respond_to?(:id) ? u.id : u
    where("daily_task_completions.user_id = #{user_id}")
  }

  scope :with_task, -> { includes(daily_task: :task) }
  scope :with_daily_task, -> { includes(:daily_task) }

  delegate  :deleted?, :full_time, :time, :name, :priority, :task, :task_id, :day,
            :total_seconds, allow_nil: true, to: :daily_task

  validates :date, presence: true
  validates :spent_seconds_db, numericality: { greater_than_or_equal_to: 0, less_than: 86_400 }

  def completion(date:)
    self
  end

  def spent_time
    hours = spent_seconds / 3600
    minutes = (spent_seconds - (hours * 3600)) / 60
    seconds = spent_seconds - (hours * 3600) - (minutes * 60)

    hours_s = hours.to_s.rjust(2, '0')
    minutes_s = minutes.to_s.rjust(2, '0')
    seconds_s = seconds.to_s.rjust(2, '0')

    "#{hours_s}:#{minutes_s}:#{seconds_s}"
  end

  def spent_seconds
    return read_attribute(:spent_seconds) if read_attribute(:spent_seconds).present?

    0
  end

  def spent_seconds_db
    read_attribute(:spent_seconds)
  end

  def as_unified_json
    {
      'id' => id,
      'date' => date,
      'name' => name,
      'time' => full_time,
      'priority' => priority,
      'spent_time' => spent_time,
      'completed' => completed?,
      'daily_task_id' => daily_task_id,
      'task_id' => task_id,
      'created_at' => created_at,
      'updated_at' => updated_at
    }
  end

  alias orig_as_json as_json

  def as_json(options = {})
    return as_unified_json if options[:unified]

    orig_as_json(methods: %i[spent_time], except: %i[user_id])
  end

  def total_spent_seconds
    completed? ? total_seconds + spent_seconds : spent_seconds
  end

  def fully_completed?
    completed? && spent_seconds >= 86_399
  end

  def human_spent_time
    return "+#{spent_time}" if completed?

    spent_time
  end

  def percent
    return ::Support::Percent.new(1.to_f) if spent_seconds > total_seconds

    ::Support::Percent.new(spent_seconds / total_seconds.to_f)
  end

  def completed?
    completed
  end

  alias completion! completion
end
