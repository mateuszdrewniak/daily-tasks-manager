# frozen_string_literal: true

class Task < ApplicationRecord
  belongs_to :user

  has_many :daily_tasks

  scope :active, -> { where('deleted IS NULL') }

  scope :active_for_user, lambda { |u|
    user_id = u.respond_to?(:id) ? u.id : u
    where("tasks.user_id = #{user_id} AND tasks.deleted IS NULL")
  }

  validates :db_minutes, numericality: { greater_than_or_equal_to: 0, less_than: 1_440 }
  validates :name, presence: true
  validates :priority, numericality: { greater_than_or_equal_to: 0, less_than: 3 }
  validates :user, presence: true
  validate :no_time

  alias orig_as_json as_json

  def as_json(options = {})
    {
      id: id,
      name: name,
      time: time,
      priority: priority,
      deleted: deleted
    }
  end

  def to_s
    name
  end

  def time
    "#{hours_s}:#{minutes_s}"
  end

  def minutes(db: false)
    minute = read_attribute(:minutes) || 0
    return minute if db

    minute - (hours * 60)
  end

  def db_minutes
    minutes(db: true)
  end

  def hours
    minute = read_attribute(:minutes) || 0
    minute / 60
  end

  def hours_s
    hours.to_s.rjust(2, '0')
  end

  def minutes_s
    minutes.to_s.rjust(2, '0')
  end

  def soft_delete
    self.deleted = true
    daily_tasks.update_all(deleted: true)

    save
  end

  private

  def no_time
    errors.add(:time, 'must be greater than 0') if db_minutes&.zero?
  end
end
