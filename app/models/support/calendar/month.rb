# frozen_string_literal: true

class ::Support::Calendar::Month
  attr_accessor :year, :month, :current_day, :offset, :number_of_days
  attr_reader   :weeks, :user

  def initialize(user, year, month, current_day, offset, number_of_days)
    @user = user
    @year = year
    @month = month
    @current_day = current_day
    @offset = offset
    @number_of_days = number_of_days
    @weeks = []

    reload
  end

  def reload
    @weeks = []
    day_number = 1
    week_number = 0
    weeks << ::Support::Calendar::Week.new

    offset.times do
      weeks[week_number].days << ::Support::Calendar::NullDay.new
    end

    while day_number <= number_of_days
      while day_number <= last_weekday(week_number) && day_number <= number_of_days
        current = current_day == day_number
        day = if day_number > current_day
                ::Support::Calendar::EmptyDay.new(user, day_date(day_number), current)
              else
                ::Support::Calendar::Day.new(user, day_date(day_number), current)
              end

        weeks[week_number].days << day
        day_number += 1
      end

      if day_number <= number_of_days
        week_number += 1
        weeks << ::Support::Calendar::Week.new
      end
    end

    while day_number <= last_weekday(week_number)
      weeks[week_number].days << ::Support::Calendar::NullDay.new
      day_number += 1
    end

    true
  end

  def as_json(options = {})
    {
      month: {
        weeks: weeks.as_json,
        number: month,
        roman_number: ::Support::Date.roman_month(month)
      }
    }
  end

  private

  def last_weekday(week_number)
    1 + (6 - offset) + 7 * week_number
  end

  def day_date(day)
    "#{year}-#{month}-#{day}"
  end
end
