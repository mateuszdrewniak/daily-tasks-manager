# frozen_string_literal: true

class ::Support::Calendar::Day
  attr_reader :user, :tasks, :iso_date, :current, :date

  def initialize(user, iso_date, current)
    @user = user
    @iso_date = iso_date
    @date = ::Date.new(year, month, day_number)
    @current = current
    @tasks = []

    reload
  end

  def reload
    @tasks = ::DailyTaskCompletion.with_task.for_user(user).where(date: iso_date).to_a
  end

  def current?
    current
  end

  def weekday
    day = date.wday - 1

    case day
    when -1
      6
    else
      day
    end
  end

  def weekend?
    date.on_weekend?
  end

  def day_number
    @day_number ||= iso_date.split('-').last.to_i
  end

  def month
    @month ||= iso_date.split('-')[1].to_i
  end

  def roman_month
    ::Support::Date.roman_month(month)
  end

  def year
    @year ||= iso_date.split('-').first.to_i
  end

  def as_json(options = {})
    {
      day_number: day_number,
      weekend: weekend?,
      month: month,
      roman_month: roman_month,
      year: year,
      current: current?,
      tasks: tasks.as_json(unified: true)
    }
  end
end
