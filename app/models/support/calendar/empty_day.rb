# frozen_string_literal: true

class ::Support::Calendar::EmptyDay < ::Support::Calendar::Day
  def reload; end
end
