# frozen_string_literal: true

class ::Support::Calendar::ThreeDays
  include ::DateHelper

  attr_reader :user, :date, :days

  def initialize(user, date)
    @user = user
    @date = date
    @days = []

    reload
  end

  def reload
    @days = []
    current_date = date
    3.times do |i|
      days << ::Support::Calendar::Day.new(user, to_iso(current_date), i.zero?)
      current_date = current_date.yesterday
    end

    @days.reverse!

    true
  end

  def as_json(options = {})
    {
      three_days: {
        days: days.as_json
      }
    }
  end
end
