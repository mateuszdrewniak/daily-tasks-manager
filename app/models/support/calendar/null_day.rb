# frozen_string_literal: true

class ::Support::Calendar::NullDay
  def tasks
    []
  end

  def current?; end

  def weekend?
    false
  end

  def as_json(options = {})
    {}
  end

  def day_number; end
end
