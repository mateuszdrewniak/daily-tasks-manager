# frozen_string_literal: true

class ::Support::Calendar::Week
  attr_reader :days

  def initialize
    @days = []
  end

  def as_json(options = {})
    {
      days: days.as_json
    }
  end
end
