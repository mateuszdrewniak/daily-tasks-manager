# frozen_string_literal: true

class Support::Simulation::Schedule
  attr_reader :tasks

  def initialize
    @next_task_id = 1
    @next_daily_task_id = 1
    @tasks = []
    @used_task_indices = []
  end

  def generate_tasks
    arr = []
    rand(1..10).times do
      arr << random_task
    end

    @tasks += arr
    arr
  end

  def generate_priority_tasks(priority: 0)
    arr = []
    rand(1..5).times do
      arr << random_task(priority: priority)
    end

    @tasks += arr
    arr
  end

  def generate_daily_tasks
    arr = []
    @used_task_indices = []

    rand(1..5).times do
      i = unique_task_index
      break unless i

      current_task = @tasks[i].dup
      task_id = current_task.id
      current_task.id = next_daily_task_id
      current_task.task_id = task_id
      arr << current_task
    end

    arr
  end

  def unique_task_index
    return unless @tasks.size > @used_task_indices.size

    i = -1

    loop do
      used = false
      i = rand(0...@tasks.size)

      @used_task_indices.each do |used_i|
        if i == used_i
          used = true
          break
        end
      end

      break unless used
    end

    @used_task_indices << i
    i
  end

  def random_task(priority: rand(3))
    OpenStruct.new(
      id: next_task_id,
      name: self.class.random_task_name,
      priority: priority,
      time: self.class.random_task_duration
    )
  end

  def self.random_task_duration
    duration = "0#{rand(0..5)}:"

    minutes = rand(1..59)
    minutes = minutes < 10 ? "0#{minutes}" : minutes.to_s
    duration += minutes

    duration
  end

  def self.random_task_name
    case rand(5)
    when 3, 4
      "#{Faker::Verb.ing_form.capitalize} #{Faker::ProgrammingLanguage.name}"
    when 2
      "#{Faker::Verb.ing_form.capitalize} #{Faker::Science.element}"
    else
      Faker::Verb.ing_form.capitalize
    end
  end

  private

  def next_task_id
    id = @next_task_id
    @next_task_id += 1

    id
  end

  def next_daily_task_id
    id = @next_daily_task_id
    @next_daily_task_id += 1

    id
  end
end
