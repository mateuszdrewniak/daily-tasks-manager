# frozen_string_literal: true

class ::Support::Colors
  GREEN = '#21ab65'
  BLUE = '#4299e1'
  ORANGE = '#f16135'

  TEXT_COLOR = {
    orange: { class: 'text-light-warning', hex: '#f8ac94' },
    blue: { class: 'text-diamond-blue', hex: '#533eff' },
    wizard_green: { class: 'text-wizard-green', hex: '#29d57e' },
    yellow: { class: 'text-sunny-yellow', hex: '#eae51f' },
    black: { class: 'text-my-black', hex: '#455962' },
    turqoise: { class: 'text-turqoise', hex: '#42edf7' },
    kiwi_green: { class: 'text-kiwi-green', hex: '#b1ffca' },
    green: { class: 'text-green', hex: '#21ab65' }
  }.freeze

  BACKGROUND_COLOR = {
    black: { class: 'bg-my-black', hex: '#455962' },
    light: { class: 'bg-light', hex: '#fafbfc' }

  }.freeze
end
