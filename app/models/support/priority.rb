# frozen_string_literal: true

class ::Support::Priority
  PRIORITIES = [0, 1, 2].freeze

  def self.collection
    array = []

    PRIORITIES.each do |num|
      array << [::I18n.translate("priorities.#{num}"), num]
    end

    array
  end

  def self.to_human(priority)
    ::I18n.translate("priorities.#{priority}")
  end
end
