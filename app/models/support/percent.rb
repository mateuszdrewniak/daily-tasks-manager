# frozen_string_literal: true

class ::Support::Percent
  attr_reader :value

  def initialize(value)
    @value = value
  end

  def to_float
    value.to_f
  end

  alias to_f to_float

  def to_str
    "#{value * 100}%"
  end

  alias to_s to_str
end
