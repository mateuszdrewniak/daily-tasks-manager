# frozen_string_literal: true

class ::Support::Date
  DAYS = (0..6).freeze
  MONTHS = (1..12).freeze

  def self.to_iso(date)
    "#{date.year}-#{date.month.to_s.rjust(2, '0')}-#{date.day.to_s.rjust(2, '0')}"
  end

  def self.now
    ::Time.zone.now
  end

  def self.today
    to_iso(now)
  end

  def self.at_beginning_of_month
    ::Time.zone.now.at_beginning_of_month
  end

  def self.at_end_of_month
    ::Time.zone.now.at_end_of_month
  end

  def self.roman_month(month)
    numeral_parts = roman_numeral_parts(month)

    numeral_parts[0] + numeral_parts[1] * numeral_parts[2]
  end

  def self.roman_numeral_parts(month)
    if month.between?(1, 3)
      ['', 'I', month]
    elsif month == 4
      ['IV', '', 0]
    elsif month.between?(5, 8)
      ['V', 'I', (month - 5)]
    elsif month == 9
      ['IX', '', 0]
    else
      ['X', 'I', (month - 10)]
    end
  end

  def self.days_each(&block)
    DAYS.each do |num|
      block.call(::I18n.translate("days.#{num}"), num)
    end
  end

  def self.days_collection
    array = []

    DAYS.each do |num|
      array << [::I18n.translate("days.#{num}"), num]
    end

    array
  end

  def self.days_hash(invert: false)
    hash = {}

    if invert
      DAYS.each do |num|
        hash[num] = ::I18n.translate("days.#{num}")
      end
    else
      DAYS.each do |num|
        hash[::I18n.translate("days.#{num}")] = num
      end
    end

    hash
  end

  def self.months_each(&block)
    MONTHS.each do |num|
      block.call(::I18n.translate("months.#{num}"), num)
    end
  end

  def self.months_collection
    array = []

    MONTHS.each do |num|
      array << [::I18n.translate("months.#{num}"), num]
    end

    array
  end

  def self.months_hash(invert: false)
    hash = {}

    if invert
      MONTHS.each do |num|
        hash[num] = ::I18n.translate("months.#{num}")
      end
    else
      MONTHS.each do |num|
        hash[::I18n.translate("months.#{num}")] = num
      end
    end

    hash
  end

  def self.weekday(date = now)
    day = date.wday - 1

    case day
    when -1
      6
    else
      day
    end
  end

  def self.human_day_of_the_week(date)
    "#{human_day(date).first(3)} #{date.to_s.split('-').last(2).join('-')}"
  end

  def self.human_day(date)
    day = date.respond_to?(:wday) ? weekday(date) : date
    ::I18n.translate("days.#{day}")
  end

  def self.human_month(month)
    ::I18n.translate("months.#{month}")
  end
end
