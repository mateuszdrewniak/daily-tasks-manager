# frozen_string_literal: true

class ::Null
  def null?
    true
  end

  def persisted?
    false
  end
end
