# frozen_string_literal: true

class User < ::ApplicationRecord
  class EmailAlreadyExistsError < ::StandardError; end

  include ::UniqueIdable

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable

  devise :omniauthable, omniauth_providers: %i[google_oauth2 facebook github]

  devise :jwt_authenticatable, jwt_revocation_strategy: ::JwtDenylist

  has_many :tasks
  has_many :daily_tasks
  has_many :daily_task_completions

  after_initialize :default_values

  scope :recent, lambda { |limit|
    all.limit(limit).order(created_at: :desc)
  }

  def self.timezones
    ::ActiveSupport::TimeZone::MAPPING
  end

  def self.timezones_inverted
    ::ActiveSupport::TimeZone::MAPPING.invert
  end

  def self.from_omniauth(auth)
    email = auth.info.email || "#{::Time.current.to_i}_#{::SecureRandom.hex(10)}@dummy.com"
    existing_user = where(email: email).first

    user = where(provider: auth.provider, uid: auth.uid).first_or_initialize do |user|
      raise(EmailAlreadyExistsError, existing_user.account_type) if existing_user.present?

      user.email = email
      user.password = ::Devise.friendly_token[0, 20]
      user.display_name = auth.info.name # assuming the user model has a name
      user.avatar_url = auth.info.image
      user.skip_confirmation!
      user.avatar_url = auth.info.image
      user.save!
    end

    if auth.info.image.present? && user.persisted? && auth.info.image != user.avatar_url
      user.avatar_url = auth.info.image
      user.save!
    end

    user
  rescue ::ActiveRecord::RecordInvalid
    ::Null::User.new
  end

  def level
    read_attribute(:level) || 0
  end

  def friendly_timezone
    self.class.timezones_inverted[timezone]
  end

  def local_time
    return ::Time.current unless timezone

    ::Time.use_zone(timezone) do
      ::Time.zone.now
    end
  end

  def blocked_timezone?
    !!blocked_timezone
  end

  def admin?
    admin
  end

  def auto_timezone
    !blocked_timezone
  end

  def avatar_source
    if read_attribute(:avatar_url).present? && provider.present?
      provider.chomp('_oauth2').inquiry
    else
      'gravatar'.inquiry
    end
  end

  def avatar_url
    return gravatar_url if avatar_source.gravatar?

    read_attribute(:avatar_url)
  end

  def gravatar_url
    hash = ::Digest::MD5.hexdigest(email)
    ::URI::HTTPS.build(host: 'www.gravatar.com', path: "/avatar/#{hash}").to_s
  end

  def account_type
    return provider.chomp('_oauth2').inquiry if provider.present?

    'regular'.inquiry
  end

  def level_object
    ::SymbolHelper.level_class(level)
  end

  alias orig_as_json as_json

  def as_json(options = {})
    orig_as_json(
      only: %i[guid email display_name level timezone avatar_url],
      methods: %i[friendly_timezone auto_timezone account_type avatar_source level_object]
    )
  end

  def display_name
    read_attribute(:display_name).presence || email_name
  end

  alias name display_name

  def email_name
    return unless email.present?

    email.split('@').first.split('.').map(&:classify).join(" ").first(50)
  end

  private

  def default_values
    return unless new_record?

    self.guid ||= generate_unique_id(column: :guid, prefix: 'usr')
  end
end
