# frozen_string_literal: true

class DailyTask < ::ApplicationRecord
  include ::DateHelper

  belongs_to :user
  belongs_to :task

  has_many :daily_task_completions

  scope :active_for_user, lambda { |u|
    user_id = u.respond_to?(:id) ? u.id : u
    where("daily_tasks.user_id = #{user_id} AND daily_tasks.deleted IS NULL")
  }

  scope :active, -> { where('daily_tasks.deleted IS NULL') }
  scope :ordered, -> { includes(:task).order('tasks.priority DESC, name ASC') }
  scope :with_task, -> { includes(:task) }

  delegate :name, :time, :minutes, :priority, to: :task

  validates :user, presence: true
  validates :task_id, numericality: { greater_than_or_equal_to: 1 }
  validates :day, numericality: { greater_than_or_equal_to: 0, less_than: 7 }

  def completion(date:)
    daily_task_completions.where(date: date).last || self
  end

  def completion!(date:)
    daily_task_completions.where(date: date).last
  end

  def spent_seconds
    0
  end

  def spent_time
    '00:00:00'
  end

  alias human_spent_time spent_time
  alias total_spent_seconds spent_seconds

  def total_seconds
    minutes(db: true) * 60
  end

  def percent
    ::Support::Percent.new(0)
  end

  def completed?
    false
  end

  alias orig_as_json as_json

  def as_json(options = {})
    methods = options[:with_task] ? %i[name time priority] : []
    orig_as_json(except: %i[user_id], methods: methods)
  end

  def as_unified_json
    {
      'id' => nil,
      'date' => today,
      'name' => name,
      'time' => full_time,
      'priority' => priority,
      'spent_time' => spent_time,
      'completed' => completed?,
      'daily_task_id' => id,
      'task_id' => task_id,
      'created_at' => nil,
      'updated_at' => nil
    }
  end

  alias fully_completed? completed?
  alias completed completed?

  def daily_task_id
    id
  end

  def soft_delete
    self.deleted = true

    save
  end

  def deleted?
    !!deleted
  end

  def full_time
    "#{time}:00"
  end
end
