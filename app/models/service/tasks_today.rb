# frozen_string_literal: true

class ::Service::TasksToday
  attr_reader :user, :date

  def initialize(user, date)
    @user = user
    @date = date
  end

  def lists
    todo = []
    done = []

    user.daily_tasks.active.where(day: day).ordered.map do |dt|
      completion = dt.completion(date: date_str)
      completion.completed? ? done << completion : todo << completion
      completion
    end

    [todo, done]
  end

  def day
    day = date.wday - 1

    case day
    when -1
      6
    else
      day
    end
  end

  def date_str
    date.to_s
  end
end
