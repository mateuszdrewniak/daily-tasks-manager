# frozen_string_literal: true

class ::Service::Calendar::Month
  include ::DateHelper

  attr_reader :date, :user

  def initialize(user, date)
    @user = user
    @date = date
  end

  def call
    year = date.year
    month = date.month
    first_weekday = weekday(date.at_beginning_of_month)
    last_day = date.at_end_of_month.day
    current_day = date.day

    ::Support::Calendar::Month.new(user, year, month, current_day, first_weekday, last_day)
  end
end
