# frozen_string_literal: true

class ::Service::Calendar::ThreeDays
  attr_reader :user, :date

  def initialize(user, date)
    @user = user
    @date = date
  end

  def call
    ::Support::Calendar::ThreeDays.new(user, date)
  end
end
