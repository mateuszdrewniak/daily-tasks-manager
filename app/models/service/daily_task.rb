# frozen_string_literal: true

class ::Service::DailyTask
  attr_accessor :task_id, :user, :day, :persisted_daily_task

  def initialize(params, user)
    @task_id = params[:task_id].to_i
    @day = params[:day].to_i
    @user = user
  end

  def create!
    ::Task.where(user: user, id: task_id).first!
    raise ::ActiveRecord::RecordInvalid if ::DailyTask.active.where(user: user, day: day, task_id: task_id).any?

    daily_task = ::DailyTask.new(**daily_task_attributes)
    daily_task.save!

    @persisted_daily_task = daily_task
  end

  private

  def daily_task_attributes
    {
      task_id: task_id,
      user: user,
      day: day
    }
  end
end
