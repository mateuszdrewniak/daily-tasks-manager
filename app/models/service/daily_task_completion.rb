# frozen_string_literal: true

class ::Service::DailyTaskCompletion
  attr_reader :user, :daily_task, :spent_time, :completed, :spent_seconds, :date, :subject, :created

  def initialize(attributes, daily_task_id, user)
    @date = attributes[:date]
    @spent_time = attributes[:spent_time]
    @completed = attributes[:completed]
    @daily_task = ::DailyTask.active.find(daily_task_id)
    @user = user
    @created = false

    set_parsed_time
  end

  def create_or_update
    @subject = daily_task.daily_task_completions.where(date: date).first

    if subject
      subject.update(spent_seconds: spent_seconds, completed: completed)
    else
      @subject = ::DailyTaskCompletion.new(
        user: user,
        daily_task: daily_task,
        spent_seconds: spent_seconds,
        date: date,
        completed: completed
      )
      subject.save!

      @created = true
    end

    subject
  end

  private

  def set_parsed_time
    return unless spent_time.is_a? ::String

    time_array = spent_time.split(':')
    return unless time_array.size == 3

    hours = time_array[0].to_i || 0
    minutes = time_array[1].to_i || 0
    minutes += (hours * 60)
    seconds = time_array[2].to_i || 0
    seconds += (minutes * 60)

    @spent_seconds = seconds
  end
end
