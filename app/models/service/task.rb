# frozen_string_literal: true

class ::Service::Task
  attr_accessor :id, :time, :name, :priority, :user, :minutes, :persisted_task

  def initialize(params, user)
    @id = params[:id]
    @time = params[:time]
    @name = params[:name]
    @priority = params[:priority_id].to_i
    @user = user

    set_parsed_time
  end

  def create!
    task = ::Task.new(**task_attributes)
    task.save!

    @persisted_task = task
  end

  def update!
    task = ::Task.where(user: user, id: id).first!
    task.assign_attributes(**task_attributes)
    task.save!

    @persisted_task = task
  end

  private

  def task_attributes
    {
      name: name,
      minutes: minutes,
      priority: priority,
      user: user
    }
  end

  def set_parsed_time
    return unless time.is_a? ::String

    time_array = time.split(':')
    return unless time_array.size == 2

    hours = time_array.first.to_i || 0
    @minutes = time_array.last.to_i || 0
    if minutes > 60
      @minutes = 0
      return
    end

    @minutes += (hours * 60)
  end
end
