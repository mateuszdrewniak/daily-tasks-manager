# frozen_string_literal: true

class ::Service::Router
  attr_reader :url_helpers, :host, :protocol

  def initialize(url_helpers:, host:, protocol:)
    @url_helpers = url_helpers
    @host = host
    @protocol = protocol
  end

  def url(name, options = {})
    return unless name.respond_to?(:to_sym) && name.respond_to?(:to_s)

    url_helpers.public_send(:"#{name}_url", host: @host, protocol: @protocol, **options)
  end

  def url_json(name, options = {})
    "#{url(name, options)}.json"
  end
end
