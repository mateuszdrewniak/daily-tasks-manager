# frozen_string_literal: true

class ::Service::User
  attr_reader :user, :blocked_timezone, :email, :display_name, :email_change, :timezone

  def initialize(params, user)
    @display_name = params[:user][:display_name]
    @email = params[:user][:email]
    @blocked_timezone = !params[:user][:auto_timezone]
    @timezone = params[:user][:timezone]
    @user = user
  end

  def update!
    @email_change = email.present? && (email != user.email)

    user.assign_attributes(
      display_name: display_name,
      email: email,
      blocked_timezone: blocked_timezone
    )

    user.timezone = timezone if blocked_timezone

    user.save!

    user
  end

  def to_h
    hash = user.as_json
    hash[:email_change] = email_change

    hash
  end
end
