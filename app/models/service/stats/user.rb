# frozen_string_literal: true

require 'set'

class ::Service::Stats::User
  extend ::Memoizable

  attr_reader :user

  def initialize(user)
    @user = user
  end

  memoize %i[hours_spent_per_task_last_week
             minutes_spent_per_task_last_week
             hours_spent_per_task_this_week
             minutes_spent_per_task_this_week
             hours_assigned_per_task_week
             minutes_assigned_per_task_week
             tasks_count_per_priority
             daily_tasks_count_per_day
             hours_spent_on_tasks_last_seven_days
             minutes_spent_on_tasks_last_seven_days
             assigned_tasks_hours_per_day_of_week
             assigned_tasks_minutes_per_day_of_week
             all_daily_tasks_count_per_day
             hours_spent_on_tasks_this_week
             minutes_spent_on_tasks_this_week
             hours_spent_on_tasks_last_week
             minutes_spent_on_tasks_last_week
             contribution_graph]

  def as_chartkick_json(_options = {})
    {
      tasks_count_per_priority: tasks_count_per_priority,
      tasks_per_day: {
        daily_tasks_count: daily_tasks_count_per_day,
        assigned_tasks_minutes: assigned_tasks_minutes_per_day_of_week,
        assigned_tasks_hours: assigned_tasks_hours_per_day_of_week
      },
      this_week: {
        minutes_spent_on_tasks: minutes_spent_on_tasks_this_week,
        minutes_spent_per_task: minutes_spent_per_task_this_week,
        hours_spent_on_tasks: hours_spent_on_tasks_this_week,
        hours_spent_per_task: hours_spent_per_task_this_week
      },
      last_week: {
        minutes_spent_on_tasks: minutes_spent_on_tasks_last_week,
        minutes_spent_per_task: minutes_spent_per_task_last_week,
        hours_spent_on_tasks: hours_spent_on_tasks_last_week,
        hours_spent_per_task: hours_spent_per_task_last_week
      }
    }
  end

  def as_json(_options = {})
    {
      tasks_count_per_priority: to_pie_chart_kit_format(tasks_count_per_priority),
      last_months: {
        contribution_graph: contribution_graph
      },
      tasks_per_day: {
        daily_tasks_count: to_bar_chart_kit_format(daily_tasks_count_per_day),
        assigned_tasks_minutes: to_bar_chart_kit_format(assigned_tasks_minutes_per_day_of_week),
        assigned_tasks_hours: to_bar_chart_kit_format(assigned_tasks_hours_per_day_of_week)
      },
      this_week: {
        minutes_spent_on_tasks: to_bar_chart_kit_format(minutes_spent_on_tasks_this_week),
        minutes_spent_per_task: to_bar_chart_kit_format(minutes_spent_per_task_this_week),
        hours_spent_on_tasks: to_bar_chart_kit_format(hours_spent_on_tasks_this_week),
        hours_spent_per_task: to_bar_chart_kit_format(hours_spent_per_task_this_week)
      },
      last_week: {
        minutes_spent_on_tasks: to_bar_chart_kit_format(minutes_spent_on_tasks_last_week),
        minutes_spent_per_task: to_bar_chart_kit_format(minutes_spent_per_task_last_week),
        hours_spent_on_tasks: to_bar_chart_kit_format(hours_spent_on_tasks_last_week),
        hours_spent_per_task: to_bar_chart_kit_format(hours_spent_per_task_last_week)
      }
    }
  end

  protected

  def __contribution_graph__
    days_last_two_months = ::DateTime.now.last_month.end_of_month.day + ::DateTime.now.end_of_month.day
    last_months = ::DailyTaskCompletion.group_by_day(:created_at, last: days_last_two_months).sum(:spent_seconds)
    last_months.map do |date, seconds|
      { date: ::Support::Date.to_iso(date), count: seconds / 60.0 }
    end
  end

  def __hours_spent_on_tasks_last_week__
    minutes_to_hours(minutes_spent_on_tasks_last_week)
  end

  def __minutes_spent_on_tasks_last_week__
    range = today.last_week...last_monday
    data_completions = ::DailyTaskCompletion.for_user(user).group_by_day(:date, range: range).sum(:spent_seconds)
    data_goal = assigned_tasks_minutes_per_day_of_week.first[:data]
    data_completions_new = {}
    data_goal_new = {}

    data_completions.each do |key, value|
      human_key = ::Support::Date.human_day_of_the_week(key)
      data_completions_new[human_key] = value / 60
      data_goal_new[human_key] = data_goal[::Support::Date.human_day(key)]
    end

    [{ data: data_completions_new, name: 'Minutes spent' }, { data: data_goal_new, name: 'Minutes assigned' }]
  end

  def __hours_spent_per_task_last_week__
    minutes_to_hours(minutes_spent_per_task_last_week)
  end

  def __minutes_spent_per_task_last_week__
    sunday_two_weeks_ago = today.last_week - 1

    data = ::DailyTaskCompletion.with_task.where("date > '#{sunday_two_weeks_ago}' AND date < '#{last_monday}'").group(:name).sum(:spent_seconds)

    data.each do |name, seconds|
      data[name] = seconds / 60
    end

    data = data.sort.to_h

    [{ data: data, name: 'Minutes spent' }, minutes_assigned_per_task_week.first]
  end

  def __hours_spent_per_task_this_week__
    minutes_to_hours(minutes_spent_per_task_this_week)
  end

  def __minutes_spent_per_task_this_week__
    last_sunday = today - today.cwday

    data = ::DailyTaskCompletion.with_task.where("date > '#{last_sunday}'").group(:name).sum(:spent_seconds)

    data.each do |name, seconds|
      data[name] = seconds / 60
    end

    data = data.sort.to_h

    [{ data: data, name: 'Minutes spent' }, minutes_assigned_per_task_week.first]
  end

  def __hours_assigned_per_task_week__
    minutes_to_hours(minutes_assigned_per_task_week)
  end

  def __minutes_assigned_per_task_week__
    data = ::DailyTask.with_task.active_for_user(user).group(:name).sum(:minutes)
    data = data.sort.to_h
    [{ data: data, name: 'Minutes assigned' }]
  end

  def __tasks_count_per_priority__
    stats = ::Task.active_for_user(user).group(:priority).count
    result = {}

    stats.each do |priority, count|
      result[::Support::Priority.to_human(priority)] = count
    end

    result
  end

  def __hours_spent_on_tasks_this_week__
    minutes_to_hours(minutes_spent_on_tasks_this_week)
  end

  def __minutes_spent_on_tasks_this_week__
    last_seven_days = minutes_spent_on_tasks_last_seven_days

    data_minutes_spent = last_seven_days.first[:data]
    data_minutes_assigned = assigned_tasks_minutes_per_day_of_week.first[:data]
    data_minutes_assigned_new = {}
    data_minutes_spent_new = {}
    last_date = nil

    data_minutes_spent.each do |date, minutes|
      next if date < last_monday

      human_date = ::Support::Date.human_day_of_the_week(date)
      data_minutes_spent_new[human_date] = minutes
      data_minutes_assigned_new[human_date] = data_minutes_assigned[::Support::Date.human_day(date)]
      last_date = date
    end

    ((last_date + 1)...(last_date.next_week)).each do |date|
      human_date = ::Support::Date.human_day_of_the_week(date)
      data_minutes_assigned_new[human_date] = data_minutes_assigned[::Support::Date.human_day(date)]
    end

    [{ data: data_minutes_spent_new, name: 'Minutes spent' }, { data: data_minutes_assigned_new, name: 'Minutes assigned' }]
  end

  def __hours_spent_on_tasks_last_seven_days__
    minutes_to_hours(minutes_spent_on_tasks_last_seven_days)
  end

  def __minutes_spent_on_tasks_last_seven_days__
    data_completions = ::DailyTaskCompletion.for_user(user).group_by_day(:date, last: 7).sum(:spent_seconds)
    data_goal = assigned_tasks_minutes_per_day_of_week.first[:data]
    data_goal_new = {}

    data_completions.each do |key, value|
      data_completions[key] = value / 60
      human_day = ::Support::Date.human_day(key)
      data_goal_new[key] = data_goal[human_day]
    end

    [{ data: data_completions, name: 'Minutes spent' }, { data: data_goal_new, name: 'Minutes assigned' }]
  end

  def __assigned_tasks_hours_per_day_of_week__
    minutes_to_hours(assigned_tasks_minutes_per_day_of_week)
  end

  def __assigned_tasks_minutes_per_day_of_week__
    per_day_decorate ::DailyTask.with_task.active_for_user(user).group(:day).sum('tasks.minutes'), 'Minutes of assigned Tasks per day'
  end

  def __daily_tasks_count_per_day__
    per_day_decorate ::DailyTask.active_for_user(user).group(:day).count, 'Tasks assigned per day'
  end

  def __all_daily_tasks_count_per_day__
    per_day_decorate ::DailyTask.where(user: user).group(:day).count, 'All Tasks assigned per day'
  end

  private

  def to_pie_chart_kit_format(data)
    return data if !data || data.empty?

    [
      {
        name: 'Low',
        count: data['Low'],
        color: ::Support::Colors::GREEN
      },
      {
        name: 'Medium',
        count: data['Medium'],
        color: ::Support::Colors::BLUE
      },
      {
        name: 'High',
        count: data['High'],
        color: ::Support::Colors::ORANGE
      }
    ]
  end

  def to_bar_chart_kit_format(data)
    return data if !data || data.empty?

    labels = ::Set.new

    data.each do |series|
      labels.merge(series[:data].keys)
    end

    labels = labels.to_a

    datasets = []
    data.each do |series|
      dataframe = []
      labels.each do |label|
        value = series[:data][label] || 0
        value.map!(&:to_f) if value.is_a?(::Array)
        dataframe << value
      end

      datasets << { data: dataframe, legend: series[:name] }
    end

    { labels: labels, datasets: datasets }
  end

  def last_monday
    last_sunday + 1
  end

  def last_sunday
    today - today.cwday
  end

  def today
    @today ||= ::Date.today
  end

  def per_day_decorate(base, name = nil)
    result = {}

    ::Support::Date.days_each do |day, num|
      result[day] = base[num] || 0
    end

    return [{ name: name, data: result }] if name

    result
  end

  def minutes_to_hours(minutes_hashes)
    hours_hashes = []

    minutes_hashes.each do |minutes_hash|
      minutes_data = minutes_hash[:data]

      hours_data = {}
      minutes_data.each do |key, value|
        hours_data[key] = (value / 60.0).round(2)
      end

      hours_name = minutes_hash[:name].gsub('Minutes', 'Hours')
      hours_hashes << { data: hours_data, name: hours_name }
    end

    hours_hashes
  end
end
