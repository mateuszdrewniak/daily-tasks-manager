# frozen_string_literal: true

class ::Service::Stats::Dashboard
  attr_reader :date

  def initialize
    @date = ::Date.current
  end

  def daily_tasks_count_per_day
    @daily_tasks_count_per_day ||= generate_daily_tasks_count_per_day
  end

  def all_daily_tasks_count_per_day
    @all_daily_tasks_count_per_day ||= generate_all_daily_tasks_count_per_day
  end

  private

  def generate_daily_tasks_count_per_day
    daily_tasks_count_per_day_decorate ::DailyTask.active.group(:day).count
  end

  def generate_all_daily_tasks_count_per_day
    daily_tasks_count_per_day_decorate ::DailyTask.group(:day).count
  end

  def daily_tasks_count_per_day_decorate(base)
    result = {}

    ::Support::Date.days_each do |day, num|
      result[day] = base[num] || 0
    end

    result
  end
end
