AppRouter = ::Service::Router.new(url_helpers: Rails.application.routes.url_helpers,
  host: Rails.application.config.default_url_options[:host],
  protocol: Rails.application.config.default_url_options[:protocol])
