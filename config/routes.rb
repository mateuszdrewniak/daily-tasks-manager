# frozen_string_literal: true

::Rails.application.routes.draw do
  devise_for :users, controllers: { omniauth_callbacks: "users/omniauth_callbacks" }
  ::ActiveAdmin.routes(self)

  get 'schedule' => 'schedule#show', as: 'show_schedule'
  get 'history' => 'history#month', as: 'show_history'
  get 'history/small' => 'history#three_days', as: 'show_history_small'
  get 'today' => 'today#show', as: 'show_today'
  get 'my_account' => 'account#show', as: 'my_account'
  get 'stats' => 'stats#show', as: 'show_stats'

  namespace :api do
    devise_scope :user do
      post '/sessions' => 'sessions#create', as: 'create_session'
      delete '/sessions' => 'sessions#destroy', as: 'destroy_session'
    end

    get '/users' => 'users#show', as: 'show_user'
    put '/users' => 'users#update', as: 'update_user'

    post '/tasks' => 'tasks#create', as: 'create_task'
    put '/tasks' => 'tasks#update', as: 'update_task'
    delete '/tasks/:task_id' => 'tasks#destroy', as: 'destroy_task'

    get '/today(/:date)' => 'today#show', as: 'show_today'

    get '/schedule' => 'schedule#show', as: 'show_schedule'

    get '/history' => 'history#month', as: 'show_history'
    get '/history/three_days' => 'history#three_days', as: 'show_history_three_days'

    get '/stats' => 'stats#show', as: 'show_stats'
    get '/stats/chartkick' => 'stats#chartkick', as: 'stats_chartkick'

    post '/daily_tasks' => 'daily_tasks#create', as: 'create_daily_task'
    put '/daily_tasks' => 'daily_tasks#update', as: 'update_daily_task'
    delete '/daily_tasks/:daily_task_id' => 'daily_tasks#destroy', as: 'destroy_daily_task'

    put '/daily_task_completions/:daily_task_id' => 'daily_task_completions#update', as: 'update_daily_task_completion'
  end

  root to: 'today#show'

  devise_scope :user do
    get 'sign_in', to: 'devise/sessions#new'
    get 'users/sign_out', to: 'devise/sessions#destroy'
  end
end
